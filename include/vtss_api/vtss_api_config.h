/*
 Vitesse Switch Software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.

*/

/* #undef VTSS_PHY_API_ONLY */

#ifndef VTSS_PHY_API_ONLY
#define VTSS_TARGET_NAME "SEVILLE"
#define VTSS_CHIP_SEVILLE
#define BOARD_SEVILLE_REF
#define VTSS_OPT_VCORE_III 0
#if defined(VTSS_OPT_VCORE_III) && VTSS_OPT_VCORE_III
#define VTSS_OPT_FDMA 1
#endif
#endif

#define VTSS_OPSYS_LINUX
#define VTSS_USE_STDINT_H
/* #undef VTSS_CHIP_10G_PHY */
#define VTSS_CHIP_CU_PHY

#define VTSS_OPT_PORT_COUNT 12

// FEATURE-DEFINES
/* #undef VTSS_FEATURE_MACSEC */

