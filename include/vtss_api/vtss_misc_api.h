/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.

 $Id$
 $Revision$

*/

/**
 * \file
 * \brief Miscellaneous API
 * \details This header file describes miscellaneous API functions
 */

#ifndef _VTSS_MISC_API_H_
#define _VTSS_MISC_API_H_

#include <vtss/api/types.h>
#include "vtss_init_api.h"
#ifdef __cplusplus
extern "C" {
#endif

/** \brief Trace group layer */
typedef enum
{
    VTSS_TRACE_LAYER_AIL,   /**< Application Interface Layer */
    VTSS_TRACE_LAYER_CIL,   /**< Chip Interface Layer */
 
    VTSS_TRACE_LAYER_COUNT  /**< Number of layers */
} vtss_trace_layer_t;

/** \brief Trace groups */
typedef enum
{
    VTSS_TRACE_GROUP_DEFAULT,       /**< Default trace group */
    VTSS_TRACE_GROUP_PORT,          /**< Port control */
    VTSS_TRACE_GROUP_PHY,           /**< PHY control */
    VTSS_TRACE_GROUP_PACKET,        /**< Packet control */
    VTSS_TRACE_GROUP_AFI,           /**< AFI */
    VTSS_TRACE_GROUP_QOS,           /**< Quality of Service */
    VTSS_TRACE_GROUP_L2,            /**< Layer 2 */
    VTSS_TRACE_GROUP_L3,            /**< Layer 3 */
    VTSS_TRACE_GROUP_SECURITY,      /**< Security */
    VTSS_TRACE_GROUP_EVC,           /**< Ethernet Virtual Connections */
    VTSS_TRACE_GROUP_FDMA_NORMAL,   /**< Frame DMA Extraction and Injection when interrupts/scheduler is enabled */
    VTSS_TRACE_GROUP_FDMA_IRQ,      /**< Frame DMA when interrupts/scheduler is disabled */
    VTSS_TRACE_GROUP_REG_CHECK,     /**< Register access errors (must be able to print when interrupts/scheduler is disabled) */
    VTSS_TRACE_GROUP_MPLS,          /**< MPLS */
    VTSS_TRACE_GROUP_HQOS,          /**< Hierarchical Quality of Service */
    VTSS_TRACE_GROUP_MACSEC,        /**< MACSEC control */
    VTSS_TRACE_GROUP_VCAP,          /**< VCAP */
    VTSS_TRACE_GROUP_OAM,           /**< OAM */
    VTSS_TRACE_GROUP_TS,            /**< Timestamping */

    VTSS_TRACE_GROUP_COUNT          /**< Number of trace groups */
} vtss_trace_group_t;

/** \brief Trace levels */
typedef enum
{
    VTSS_TRACE_LEVEL_NONE,  /**< No trace */
    VTSS_TRACE_LEVEL_ERROR, /**< Error trace */
    VTSS_TRACE_LEVEL_INFO,  /**< Information trace */
    VTSS_TRACE_LEVEL_DEBUG, /**< Debug trace */
    VTSS_TRACE_LEVEL_NOISE, /**< More debug information */

    VTSS_TRACE_LEVEL_COUNT  /**< Number of trace levels */
} vtss_trace_level_t;

/** \brief Trace group configuration */
typedef struct
{
    vtss_trace_level_t level[VTSS_TRACE_LAYER_COUNT]; /**< Trace level per layer */
} vtss_trace_conf_t;

/**
 * \brief Get trace configuration
 *
 * \param group [IN]  Trace group
 * \param conf [OUT]  Trace group configuration.
 *
 * \return Return code.
 **/
vtss_rc vtss_trace_conf_get(const vtss_trace_group_t  group,
                            vtss_trace_conf_t         *const conf);


/**
 * \brief Set trace configuration
 *
 * \param group [IN]  Trace group
 * \param conf [IN]   Trace group configuration.
 *
 * \return Return code.
 **/
vtss_rc vtss_trace_conf_set(const vtss_trace_group_t  group,
                            const vtss_trace_conf_t   *const conf);


/**
 * \brief Trace callout function
 *
 * \param layer [IN]     Trace layer
 * \param group [IN]     Trace group
 * \param level [IN]     Trace level
 * \param file [IN]      File name string
 * \param line [IN]      Line number in file
 * \param function [IN]  Function name string
 * \param format [IN]    Print format string
 *
 * \return Nothing.
 **/
void vtss_callout_trace_printf(const vtss_trace_layer_t  layer,
                               const vtss_trace_group_t  group,
                               const vtss_trace_level_t  level,
                               const char                *file,
                               const int                 line,
                               const char                *function,
                               const char                *format,
                               ...)
#if defined(__GNUC__) && (__GNUC__ > 2)
    __attribute__ ((format(printf,7,8)))
#endif
;

/**
 * \brief Trace hex-dump callout function
 *
 * \param layer [IN]     Trace layer
 * \param group [IN]     Trace group
 * \param level [IN]     Trace level
 * \param file [IN]      The file from where the trace were called.
 * \param line [IN]      The line from where the trace were called.
 * \param function [IN]  The function from where the trace were called.
 * \param byte_p [IN]    Pointer to start of area to print
 * \param byte_cnt [IN]  Number of bytes to print
 *
 * \return Nothing.
 **/
void vtss_callout_trace_hex_dump(const vtss_trace_layer_t  layer,
                                 const vtss_trace_group_t  group,
                                 const vtss_trace_level_t  level,
                                 const char                *file,
                                 const int                 line,
                                 const char                *function,
                                 const u8                  *byte_p,
                                 const int                 byte_cnt);

/** \brief Debug layer */
/* Debug layer */
typedef enum {
    VTSS_DEBUG_LAYER_ALL, /**< All layers */
    VTSS_DEBUG_LAYER_AIL, /**< Application Interface Layer */
    VTSS_DEBUG_LAYER_CIL  /**< Chip Interface Layer */
} vtss_debug_layer_t;
 
/** \brief Debug function group */
typedef enum {
    VTSS_DEBUG_GROUP_ALL,       /**< All groups */
    VTSS_DEBUG_GROUP_INIT,      /**< Initialization */
    VTSS_DEBUG_GROUP_MISC,      /**< Miscellaneous */
    VTSS_DEBUG_GROUP_PORT,      /**< Port configuration */
    VTSS_DEBUG_GROUP_PORT_CNT,  /**< Port counters */
    VTSS_DEBUG_GROUP_PHY,       /**< PHY */
    VTSS_DEBUG_GROUP_VLAN,      /**< VLAN */
    VTSS_DEBUG_GROUP_PVLAN,     /**< PVLAN */
    VTSS_DEBUG_GROUP_MAC_TABLE, /**< MAC address table */
    VTSS_DEBUG_GROUP_ACL,       /**< ACL */
    VTSS_DEBUG_GROUP_QOS,       /**< QoS */
    VTSS_DEBUG_GROUP_AGGR,      /**< Link aggregation */
    VTSS_DEBUG_GROUP_GLAG,      /**< Global link aggregation */
    VTSS_DEBUG_GROUP_STP,       /**< Spanning Tree */
    VTSS_DEBUG_GROUP_MIRROR,    /**< Mirroring */
    VTSS_DEBUG_GROUP_EVC,       /**< EVC */
    VTSS_DEBUG_GROUP_ERPS,      /**< ERPS */
    VTSS_DEBUG_GROUP_EPS,       /**< EPS */
    VTSS_DEBUG_GROUP_PACKET,    /**< Packet control */
    VTSS_DEBUG_GROUP_FDMA,      /**< FDMA */
    VTSS_DEBUG_GROUP_TS,        /**< TS: TimeStamping */
    VTSS_DEBUG_GROUP_PHY_TS,    /**< PHY_TS: PHY TimeStamping */
    VTSS_DEBUG_GROUP_WM,        /**< WaterMarks */
    VTSS_DEBUG_GROUP_LRN,       /**< LRN:COMMON */
    VTSS_DEBUG_GROUP_IPMC,      /**< IP Multicast */
    VTSS_DEBUG_GROUP_STACK,     /**< Stacking */
    VTSS_DEBUG_GROUP_CMEF,      /**< Congestion Management */
    VTSS_DEBUG_GROUP_HOST,      /**< CE-MAX Host configuration */
    VTSS_DEBUG_GROUP_MPLS,      /**< MPLS */
    VTSS_DEBUG_GROUP_MPLS_OAM,  /**< MPLS OAM */
    VTSS_DEBUG_GROUP_HQOS,      /**< Hierarchical Quality of Service */
    VTSS_DEBUG_GROUP_VXLAT,     /**< VLAN Translation */
    VTSS_DEBUG_GROUP_OAM,       /**< OAM, incl. VOEs/VOP */
    VTSS_DEBUG_GROUP_SER_GPIO,  /**< Serial GPIO configuration */
    VTSS_DEBUG_GROUP_L3,        /**< L3 services */
    VTSS_DEBUG_GROUP_AFI,       /**< Automatic Frame Injector */
    VTSS_DEBUG_GROUP_MACSEC,    /**< 802.1AE MacSec */

    /* New groups are added above this line */
    VTSS_DEBUG_GROUP_COUNT      /**< Number of groups */
} vtss_debug_group_t;

/** \brief Special chip number value for showing information from all chips */
#define VTSS_CHIP_NO_ALL 0xffffffff

/** \brief Debug information structure */
typedef struct {
    vtss_debug_layer_t layer;                           /**< Layer */
    vtss_debug_group_t group;                           /**< Function group */
    vtss_chip_no_t     chip_no;                         /**< Chip number, multi-chip targets */
    BOOL               port_list[VTSS_PORT_ARRAY_SIZE]; /**< Port list */
    BOOL               full;                            /**< Full information dump */
    BOOL               clear;                           /**< Clear counters */
} vtss_debug_info_t;
 
/** \brief Debug printf function */
#if defined(__GNUC__) && (__GNUC__ > 2)
typedef void (* vtss_debug_printf_t)(const char *fmt, ...) __attribute__ ((format(printf,1,2)));
#else
typedef void (* vtss_debug_printf_t)(const char *fmt, ...);
#endif /* __GNUC__ */
 
/**
 * \brief Get default debug information structure
 *
 * \param info [OUT]  Debug information
 *
 * \return Return code.
 **/
vtss_rc vtss_debug_info_get(vtss_debug_info_t *const info);
 
/**
 * \brief Print default information
 *
 * \param inst [IN]   Target instance reference.
 * \param prntf [IN]  Debug printf function.
 * \param info [IN]   Debug information
 *
 * \return Return code.
 **/
vtss_rc vtss_debug_info_print(const vtss_inst_t         inst,
                              const vtss_debug_printf_t prntf,
                              const vtss_debug_info_t   *const info);

/* - API protection functions -------------------------------------- */

/** \brief API lock structure */
typedef struct {
    vtss_inst_t inst;     /**< Target instance reference */
    const char *function; /**< Function name */
    const char *file;     /**< File name */
    int        line;      /**< Line number */
} vtss_api_lock_t;

/**
 * \brief Lock API access
 *
 * \param lock [IN]  Lock information
*/
void vtss_callout_lock(const vtss_api_lock_t *const lock);

/**
 * \brief Unlock API access
 *
 * \param lock [IN]  Lock information
*/
void vtss_callout_unlock(const vtss_api_lock_t *const lock);

/** \brief API debug lock structure */
typedef struct {
    vtss_chip_no_t chip_no; /**< Chip number (if multi-chip instance). */
} vtss_debug_lock_t;

/**
 * \brief Debug lock API access
 *
 * \param inst [IN]  Target instance reference.
 * \param lock [IN]  Lock information
 *
 * \return Return code.
*/
vtss_rc vtss_debug_lock(const vtss_inst_t       inst,
                        const vtss_debug_lock_t *const lock);

/**
 * \brief Debug unlock API access
 *
 * \param inst [IN]  Target instance reference.
 * \param lock [IN]  Lock information
 *
 * \return Return code.
*/
vtss_rc vtss_debug_unlock(const vtss_inst_t inst,
                          vtss_debug_lock_t *const lock);

/* - Direct register access (for debugging only) ------------------- */

/**
 * \brief Read value from target register.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param addr [IN]     Address to read. Format depends on target.
 * \param value [OUT]   Register value.
 *
 * \return Return code.
 **/
vtss_rc vtss_reg_read(const vtss_inst_t    inst,
                      const vtss_chip_no_t chip_no,
                      const u32            addr,
                      u32                  *const value);


/**
 * \brief Write value to target register.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param addr [IN]     Address to read. Format depends on target.
 * \param value [IN]    Register value.
 *
 * \return Return code.
 **/
vtss_rc vtss_reg_write(const vtss_inst_t    inst,
                       const vtss_chip_no_t chip_no,
                       const u32            addr,
                       const u32            value);


/**
 * \brief Read, modify and write value to target register.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param addr [IN]     Address to read. Format depends on target.
 * \param value [IN]    Register value.
 * \param mask [IN]     Register mask, only bits enabled are changed.
 *
 * \return Return code.
 **/
vtss_rc vtss_reg_write_masked(const vtss_inst_t    inst,
                              const vtss_chip_no_t chip_no,
                              const u32            addr,
                              const u32            value,
                              const u32            mask);

/* - Secondary chip if ------------------- */

/**
 * \brief Clear EXT0-1 interrupt sticky bits on secondary chip.
 *
 * \param inst [IN]    Target instance reference.
 * \param ext [IN]     EXT number (0-1).
 *
 * \return Return code.
 **/
vtss_rc vtss_intr_sticky_clear(const vtss_inst_t    inst,
                               u32                  ext);

/* - Chip ID and revision ------------------------------------------ */

/** \brief Chip ID */
typedef struct
{
    u16  part_number;   /**< BCD encoded part number */
    u16  revision;      /**< Chip revision */
} vtss_chip_id_t;

/**
 * \brief Get chip ID and revision.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_id [IN]  Pointer to chip ID structure.
 *
 * \return Return code.
 **/
vtss_rc vtss_chip_id_get(const vtss_inst_t  inst,
                         vtss_chip_id_t     *const chip_id);


/* - Polling functions ---------------------------------------- */

/**
 * \brief Polling function called every second.
 *
 * \param inst [IN]  Target instance reference.
 *
 * \return Return code.
 **/
vtss_rc vtss_poll_1sec(const vtss_inst_t  inst);

/**
 * \brief Define event (interrupt) types relatesd to PTP in the switch chips
 *
 **/
typedef enum {
    VTSS_PTP_SYNC_EV =      (1 << 0), /*< PTP Synchronization pulse update */
    VTSS_PTP_EXT_SYNC_EV =  (1 << 1), /*< PTP External Synchronization input  (Sync 0 on Serval)*/
    VTSS_PTP_CLK_ADJ_EV =   (1 << 2), /*< PTP Clock adjustment updated */
    VTSS_PTP_TX_TSTAMP_EV = (1 << 3), /*< Tx timestamp udated (Jaguar and Serval only)*/
    VTSS_PTP_EXT_1_SYNC_EV =(1 << 4), /*< PTP External Synchronization 1 input (Serval only)*/
} vtss_ptp_event_type_t;

/**
 * \brief PTP polling function called at by interrupt or periodicly
 *
 * \param inst       [IN]  Target instance reference.
 * \param ev_mask    [OUT] Event type mask of active events
 *
 * \note The \e ev_mask parameter can be either a single event_type or
 * multiple event types (or \e VTSS_EVTYPE_ALL). If invoked by a
 * processor interrupt signal, the type of event to check for may be
 * narrowed in to specific events.
 *
 * \return Return code.
 **/
vtss_rc vtss_ptp_event_poll(const vtss_inst_t      inst,
                            vtss_ptp_event_type_t  *const ev_mask);

/**
 * \brief Enable PTP event generation for a specific event type
 *
 * \param inst     [IN] Target instance reference.
 * \param enable   [IN] Enable or disable events
 * \param ev_mask  [IN] Event type(s) to control (mask)
 *
 * \return Return code.
 **/
vtss_rc vtss_ptp_event_enable(const vtss_inst_t            inst,
                              const vtss_ptp_event_type_t  ev_mask,
                              const BOOL                   enable);

/**
 * \brief Define the dev_all event (interrupt) types 
 *
 **/
typedef enum {
    VTSS_DEV_ALL_TX_TSTAMP_EV = (1 << 0), /*< Tx timestamp udated */
    VTSS_DEV_ALL_LINK_EV =      (1 << 1), /*< Link state changed */
} vtss_dev_all_event_type_t;

/**
 * \brief Define the dev_all polling types 
 *
 **/
typedef enum {
    VTSS_DEV_ALL_POLL_ALL,          /*< Polling all chip */
    VTSS_DEV_ALL_POLL_PRIMARY,      /*< Polling primary chip */
    VTSS_DEV_ALL_POLL_SECONDARY,    /*< Polling secondary chip */
} vtss_dev_all_event_poll_t;

/**
 * \brief DEV_ALL polling function called at by interrupt or periodically
 *
 * \param inst      [IN] Target instance reference.
 * \param poll_type [IN] Polling type
 * \param ev_mask   [OUT] Event type mask array of active events for all ports - must be of size VTSS_PORT_ARRAY_SIZE
 *
 * \note The \e ev_mask parameter can be either a single event_type or
 * multiple event types (or \e VTSS_EVTYPE_ALL). If invoked by a
 * processor interrupt signal, the type of event to check for may be
 * narrowed in to specific events.
 *
 * \return Return code.
 **/
vtss_rc vtss_dev_all_event_poll(const vtss_inst_t                 inst,
                                const vtss_dev_all_event_poll_t   poll_type,
                                vtss_dev_all_event_type_t         *const ev_mask);

/**
 * \brief Enable DEV_ALL event generation for a specific event type
 *
 * \param inst     [IN] Target instance reference.
 * \param port_no  [IN] Port number.
 * \param enable   [IN] Enable or disable events.
 * \param ev_mask  [IN] Event type(s) to control (mask).
 *
 * \return Return code.
 **/
vtss_rc vtss_dev_all_event_enable(const vtss_inst_t                inst,
                                  const vtss_port_no_t             port_no,
                                  const vtss_dev_all_event_type_t  ev_mask,
                                  const BOOL                       enable);

/* - GPIOs --------------------------------------------------------- */

#if defined(VTSS_GPIOS)

/** \brief GPIO number */
typedef u32 vtss_gpio_no_t;

/** \brief GPIO start number */
#define VTSS_GPIO_NO_START 0

/** \brief GPIO end number */
#define VTSS_GPIO_NO_END   (VTSS_GPIO_NO_START+VTSS_GPIOS)

/**
 * \brief GPIO configured mode
 **/
typedef enum
{
    VTSS_GPIO_OUT,    /**< Output enabled */
    VTSS_GPIO_IN,     /**< Input enabled */
    VTSS_GPIO_IN_INT, /**< Input enabled, IRQ gated */
    VTSS_GPIO_ALT_0,  /**< Alternate function 0 */
    VTSS_GPIO_ALT_1,  /**< Alternate function 1 */
    VTSS_GPIO_ALT_2,  /**< Alternate function 2 */
} vtss_gpio_mode_t;

/**
 * \brief Set GPIO mode.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param gpio_no [IN]  GPIO pin number.
 * \param mode [IN]     GPIO mode.
 *
 * \return Return code.
 **/
vtss_rc vtss_gpio_mode_set(const vtss_inst_t      inst,
                           const vtss_chip_no_t   chip_no,
                           const vtss_gpio_no_t   gpio_no,
                           const vtss_gpio_mode_t mode);

/**
 * \brief Set GPIO direction to input or output.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param gpio_no [IN]  GPIO pin number.
 * \param output [IN]   TRUE if output, FALSE if input.
 *
 * \return Return code.
 *
 * \em DEPRECATED. Use vtss_gpio_mode_set() instead.
 **/
vtss_rc vtss_gpio_direction_set(const vtss_inst_t     inst,
                                const vtss_chip_no_t  chip_no,
                                const vtss_gpio_no_t  gpio_no,
                                const BOOL            output);



/**
 * \brief Read from GPIO input pin.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param gpio_no [IN]  GPIO pin number.
 * \param value [OUT]   TRUE if pin is high, FALSE if it is low.
 *
 * \return Return code.
 **/
vtss_rc vtss_gpio_read(const vtss_inst_t     inst,
                       const vtss_chip_no_t  chip_no,
                       const vtss_gpio_no_t  gpio_no,
                       BOOL                  *const value);



/**
 * \brief Write to GPIO output pin.
 *
 * \param inst [IN]     Target instance reference.
 * \param chip_no [IN]  Chip number (if multi-chip instance).
 * \param gpio_no [IN]  GPIO pin number.
 * \param value [IN]    TRUE to set pin high, FALSE to set pin low.
 *
 * \return Return code.
 **/
vtss_rc vtss_gpio_write(const vtss_inst_t     inst,
                        const vtss_chip_no_t  chip_no,
                        const vtss_gpio_no_t  gpio_no,
                        const BOOL            value);

/**
 * \brief Get GPIO event indication
 *
 * \param inst [IN]         Target instance reference.
 * \param chip_no [IN]      Chip number (if multi-chip instance).
 * \param events [OUT]      Event indication for each GPIO pin - must point to VTSS_GPIOS of BOOL.
 *
 * \return Return code.
 **/
vtss_rc vtss_gpio_event_poll(const vtss_inst_t        inst,
                             const vtss_chip_no_t     chip_no,
                             BOOL                     *const events);

/**
 * \brief Set GPIO event enable
 *
 * \param inst [IN]         Target instance reference.
 * \param chip_no [IN]      Chip number (if multi-chip instance).
 * \param gpio_no [IN]      GPIO pin number.
 * \param enable  [IN]      Enable or disable event.
 *
 * \return Return code.
 **/
vtss_rc vtss_gpio_event_enable(const vtss_inst_t       inst,
                               const vtss_chip_no_t    chip_no,
                               const vtss_gpio_no_t    gpio_no,
                               BOOL                    enable);
#endif /* GPIOS */



/* - Serial LED ---------------------------------------------------- */

#if defined(VTSS_FEATURE_SERIAL_LED)
/**
 * \brief LED mode
 **/
typedef enum
{
    VTSS_LED_MODE_IGNORE,   /**< Do not change */
    VTSS_LED_MODE_DISABLED, /**< Disabled */
    VTSS_LED_MODE_OFF,      /**< Off */
    VTSS_LED_MODE_ON,       /**< On */
    VTSS_LED_MODE_2_5,      /**< 2.5 Hz */
    VTSS_LED_MODE_5,        /**< 5 Hz */
    VTSS_LED_MODE_10,       /**< 10 Hz */
    VTSS_LED_MODE_20        /**< 20 Hz */
} vtss_led_mode_t;

/**
 * \brief LED port number
 **/
typedef u32 vtss_led_port_t;

/**
 * \brief Setup serial LED mode.
 *
 * \param inst [IN]  Target instance reference.
 * \param port [IN]  Serial LED port, 0-29.
 * \param mode [IN]  Serial LED mode for three LEDs.
 *
 * \return Return code.
 **/
vtss_rc vtss_serial_led_set(const vtss_inst_t      inst,
                            const vtss_led_port_t  port,
                            const vtss_led_mode_t  mode[3]);



/**
 * \brief Setup serial LED light intensity.
 *
 * \param inst [IN]  Target instance reference.
 * \param port [IN]  Serial LED port, 0-29.
 * \param intensity [IN]  intensity in percent (0-100).
 *
 * \return Return code.
 **/
vtss_rc vtss_serial_led_intensity_set(const vtss_inst_t      inst,
                                      const vtss_led_port_t  port,
                                      const i8               intensity);


/**
 * \brief Getting current serial LED light intensity.
 *
 * \param inst [IN]  Target instance reference.
 * \param intensity [IN]  intensity in percent (0-100).
 *
 * \return Return code.
 **/
vtss_rc vtss_serial_led_intensity_get(const vtss_inst_t      inst,
                                      i8                     *intensity);


#endif /* VTSS_FEATURE_SERIAL_LED */


/* - Serial GPIO control ---------------------------------------------------- */


/**
 * \brief Configure interrupt.
 * \param inst [IN] Target instance reference.
 * \param mask [IN] Interrupt mask - Configures the interrupts for the bits set in the mask. 
 * \param polarity [IN] Polarity - Interrupt polarity.
 * \param enable [IN] Enable   - 1 = enable, 0 = disable.
 *
 * \return Return code.
**/
vtss_rc vtss_intr_cfg(const vtss_inst_t       inst,
                      const u32               mask,
                      const BOOL              polarity,
                      const BOOL              enable);


/**
 * \brief Interrupt sources.
 */
typedef enum
{
    VTSS_IRQ_XTR,               /**< Frame Extraction Ready(register-based) */
    VTSS_IRQ_FDMA_XTR,          /**< Frame Extraction Ready (FDMA-based) */
    VTSS_IRQ_SOFTWARE,          /**< Software IRQ */
    VTSS_IRQ_PTP_RDY,           /**< PTP Timestamp Ready */
    VTSS_IRQ_PTP_SYNC,          /**< PTP Synchronization IRQ */
    VTSS_IRQ_EXT1,              /**< EXT1 IRQ */
    VTSS_IRQ_OAM,               /**< OAM IRQ */
    VTSS_IRQ_MAX,               /**< Maximum IRQ Source */
} vtss_irq_t;

/**
 * \brief Interrupt configuration options.
 */
typedef struct {
    BOOL external;               /**< Redirect to external IRQ */
    u8   destination;            /**< IRQ destination index */
} vtss_irq_conf_t;

/**
 * \brief Interrupt status structure.
 */
typedef struct {
    u32 active;         /**< Bitmap for pending IRQs (VTSS_IRQ_xxx) */
    u32 raw_ident;      /**< RAW (target dependentant) bitmap for active pending IRQs */
    u32 raw_status;     /**< RAW (target dependentant) bitmap for all pending IRQs */
    u32 raw_mask;       /**< RAW (target dependentant) bitmap for IRQs mask */
} vtss_irq_status_t;

/**
 * \brief Get IRQ configuration
 *
 * \param inst [IN]     Target instance reference.
 * \param irq  [IN]     Interrupt source.
 * \param conf [OUT]    IRQ configuration.
 *
 * \return Return code.
 **/
vtss_rc  vtss_irq_conf_get(const vtss_inst_t inst,
                           const vtss_irq_t irq,
                           vtss_irq_conf_t *conf);

/**
 * \brief Set IRQ configuration
 *
 * \param inst [IN]     Target instance reference.
 * \param irq  [IN]     Interrupt source.
 * \param conf [IN]     IRQ configuration.
 *
 * \return Return code.
 **/
vtss_rc  vtss_irq_conf_set(const vtss_inst_t inst,
                           const vtss_irq_t irq,
                           const vtss_irq_conf_t *const conf);

/**
 * \brief Get IRQ status (active sources), mask current sources
 *
 * \param inst   [IN]   Target instance reference.
 * \param status [OUT]  IRQ status.
 *
 * \return Return code.
 **/
vtss_rc  vtss_irq_status_get_and_mask(const vtss_inst_t inst,
                                      vtss_irq_status_t *status);

/**
 * \brief Control a specific interrupt source
 *
 * \param inst   [IN]   Target instance reference.
 * \param irq    [IN]   Interrupt source.
 * \param enable [IN]   Enable or disable source.
 *
 * \return Return code.
 **/
vtss_rc  vtss_irq_enable(const vtss_inst_t inst,
                         const vtss_irq_t irq,
                         BOOL enable);


/* - API tod functions -------------------------------------- */

/**
 * \brief Get the current hw nanosec time
 *  This function is called from interrupt
 * \returns actual ns counter
 */
u32 vtss_tod_get_ns_cnt(void);

/**
 * \brief If the actual HW does not support time stamping, an external callback
 * function can be set up to do the work.
 * \returns actual ns counter.
 */
typedef u32 (*tod_get_ns_cnt_cb_t)(void);  /* Get ns Counter callback  */

/**
 * \brief Set an external hw nanosec read function.
 *
 * \param cb pointer to callback function
 */
void vtss_tod_set_ns_cnt_cb(tod_get_ns_cnt_cb_t cb);

/**
 * VTSS_OS_TIMESTAMP_TYPE
 * VTSS_OS_TIMESTAMP()
 *   These two provides a mean to have the API timestamp events for use
 *   by the application. It is up to the platform specific code to
 *   implement the actual functions to obtain the timestamp.
 *   The implementation *must* be callable from interrupt context, so no
 *   implicit waits or sleeps are allowed.
 */
typedef struct {
    unsigned int hw_cnt; /**< hardware counter */
} vtss_os_timestamp_t;
#define VTSS_OS_TIMESTAMP_TYPE vtss_os_timestamp_t /**< The VTSS_OS_TIME_STAMP_TYPE defines the type */
#define VTSS_OS_TIMESTAMP(timestamp)                                      do {                                                                      /* Currently no need to lock scheduler, since it's only          */     /* called from a function, where the sceduler is already locked. */     /* cyg_scheduler_lock(__FILE__, __LINE__); */                                             (timestamp)->hw_cnt = vtss_tod_get_ns_cnt();                            /* cyg_scheduler_unlock(__FILE__, __LINE__); */                                         } while(0); /**< VTSS_OS_TIMESTAMP() provides the implementation that will fill in the timestamp. */
 

/* - Fan control functions -------------------------------------- */

/* - EEE functions -------------------------------------- */
/** \brief EEE port configuration */
typedef struct
{
    BOOL eee_ena;          /**< Enable EEE                                                                                                                                                       */
    u8   eee_fast_queues;  /**< Queues set in this mask will activate egress path as soon as any data is available. Vector for enabling fast queues. bit 0 = queue 0, bit 1 = queue 1 and so on. */
    u16  tx_tw;            /**< Time from path is activated until frame transmission restarted.                                                                                                  */
    u8   lp_advertisement; /**< Link partner EEE advertisement. Bit 0 = Link partner advertises 100BASE-T capability. Bit 1 = Link partner advertises 1000BASE-T capability.                     */

    BOOL optimized_for_power; /**< EEE can be optimized for either most power savings or least traffic latency */
} vtss_eee_port_conf_t;

/** \brief EEE port state change what? (JR only) */
typedef enum {
    VTSS_EEE_STATE_SELECT_LPI,      /**< Change LPI signal.               */
    VTSS_EEE_STATE_SELECT_SCH,      /**< Change scheduler enable.         */
    VTSS_EEE_STATE_SELECT_FP,       /**< Change frame mirroring flag.     */
    VTSS_EEE_STATE_SELECT_INTR_ENA, /**< Enable analyzer interrupts.      */
    VTSS_EEE_STATE_SELECT_INTR_ACK, /**< Acknowledge analyzer interrupts. */
} vtss_eee_state_select_t;

/** \brief EEE port state (JR only) */
typedef struct
{
    vtss_eee_state_select_t select; /**< State to change.                                       */
    u32                     val;    /**< New value to apply. Interpretation depends on #select. */
} vtss_eee_port_state_t;

/** \brief EEE port counters (JR only) */
typedef struct
{
    BOOL fill_level_get;   /**< [IN]  FALSE => Don't get fill level. TRUE => Get fill level.                    */
    u32  fill_level_thres; /**< [IN]  Stop iterating over queues when fill level exceeds this value.            */
    u32  fill_level;       /**< [OUT] Accumulated fill level, updated by API if #fill_level_get is TRUE.        */
    BOOL tx_out_bytes_get; /**< [IN]  FALSE => Don't get transmitted bytes. TRUE => Get tx'd bytes.             */
    u32  tx_out_bytes;     /**< [OUT] Transmitted number of bytes, updated by API if #tx_out_bytes_get is TRUE. */
} vtss_eee_port_counter_t;

/**
 * \brief Set EEE configuration
 *
 * \param inst     [IN] Target instance reference
 * \param port_no  [IN] Port number
 * \param eee_conf [IN] EEE configuration
 *
 * \return Return code.
 **/
vtss_rc vtss_eee_port_conf_set(const vtss_inst_t          inst,
                               const vtss_port_no_t       port_no,
                               const vtss_eee_port_conf_t *const eee_conf);

/**
 * \brief Change EEE Port state.
 *
 * Supported on JR only.
 *
 * \param inst      [IN] Target instance reference
 * \param port_no   [IN] Port number
 * \param eee_state [IN] New port state
 *
 * \return Return code.
 **/
vtss_rc vtss_eee_port_state_set(const vtss_inst_t                  inst,
                                const vtss_port_no_t               port_no,
                                const vtss_eee_port_state_t *const eee_state);

/**
 * \brief Get EEE-related port counters.
 *
 * Support on JR only.
 *
 * \param inst        [IN]    Target instance reference
 * \param port_no     [IN]    Port number
 * \param eee_counter [INOUT] Structure indicating which counters to get, and the returned counter value.
 *
 * \return Return code.
 */
vtss_rc vtss_eee_port_counter_get(const vtss_inst_t                    inst,
                                  const vtss_port_no_t                 port_no,
                                        vtss_eee_port_counter_t *const eee_counter);


/* - Daytona  functions -------------------------------------- */

/**
 * \brief Enable or disable register access checking.
 *
 * When enabled, every call to the register read/write
 * callouts (init_conf.reg_read()/write()) will be verified
 * for success. This will slow-down execution, so it is recommended
 * only to use this feature in dedicated debug builds.
 *
 * In multi-chip targets, only chip number 0 will be verified.
 *
 * The number of calls will be reference counted, according to
 * the following rules:
 * 1) Calls with enable = FALSE will increase the reference count.
 * 2) Calls with enable = TRUE will decrease the reference count,
 *    which cannot go lower than 0.
 * The reference count is initialized to 1 at API instantiation,
 * effectively disabling register access checking. A reference count of
 * 0 enables the feature.
 *
 * Error indications get printed with a call to
 * VTSS_EG(VTSS_TRACE_GROUP_REG_CHECK, ...), which will request
 * to be interrupt/scheduler disabled tolerant.
 *
 * Notice that this feature may not be available on all platforms.
 *
 * This feature will not work if the API is instantiated more than once,
 * since it uses the default instance to find its state.
 *
 * \param inst   [IN] Target instance reference.
 * \param enable [IN] Enable or disable register access checking (ref. counted).
 *
 * \return Return code.
 **/
vtss_rc vtss_debug_reg_check_set(const vtss_inst_t inst,
                                 const BOOL        enable);

#ifdef __cplusplus
}
#endif
#endif /* VTSS_MISC_API_H_ */
