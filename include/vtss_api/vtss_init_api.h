/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.
 
 $Id$
 $Revision$

*/

/**
 * \file
 * \brief Initialization API
 * \details This header file describes functions used to create and initialize targets
 */

#ifndef _VTSS_INIT_API_H_
#define _VTSS_INIT_API_H_

#include <vtss/api/types.h>
#ifdef __cplusplus
extern "C" {
#endif

/** \brief Target chip type */
typedef enum {
    VTSS_TARGET_CU_PHY,                       /**< Cu PHY family */
    VTSS_TARGET_10G_PHY,                      /**< 10G PHY family */
    VTSS_TARGET_BARRINGTON_II      = 0x7344,  /**< Barrington-II MAC */
    VTSS_TARGET_SCHAUMBURG_II      = 0x7346,  /**< Schaumburg-II MAC */
    VTSS_TARGET_EXEC_1             = 0x7350,  /**< eXEC1 MAC */
    VTSS_TARGET_SPARX_II_16        = 0x7401,  /**< SparX-II-16 switch */
    VTSS_TARGET_SPARX_II_24        = 0x7405,  /**< SparX-II-24 switch */
    VTSS_TARGET_E_STAX_34          = 0x7407,  /**< E-StaX-34 switch */
    VTSS_TARGET_SPARX_III_11       = 0x7414,  /**< SparX-III-11 SME switch */
    VTSS_TARGET_SERVAL_LITE        = 0x7416,  /**< Serval Lite CE switch */
    VTSS_TARGET_SERVAL             = 0x7418,  /**< Serval CE switch */
    VTSS_TARGET_SEVILLE            = 0x9953,  /**< Seville switch */
    VTSS_TARGET_SPARX_III_10_UM    = 0x7420,  /**< SparxIII-10 unmanaged switch */
    VTSS_TARGET_SPARX_III_17_UM    = 0x7421,  /**< SparxIII-17 unmanaged switch */
    VTSS_TARGET_SPARX_III_25_UM    = 0x7422,  /**< SparxIII-25 unmanaged switch */
    VTSS_TARGET_CARACAL_LITE       = 0x7423,  /**< Caracal-Lite CE switch */
    VTSS_TARGET_SPARX_III_10       = 0x7424,  /**< SparxIII-10 switch */
    VTSS_TARGET_SPARX_III_18       = 0x7425,  /**< SparxIII-18 switch */
    VTSS_TARGET_SPARX_III_24       = 0x7426,  /**< SparxIII-24 switch */
    VTSS_TARGET_SPARX_III_26       = 0x7427,  /**< SparxIII-26 switch */
    VTSS_TARGET_SPARX_III_10_01    = 0x17424, /**< SparxIII-10-01 switch */
    VTSS_TARGET_CARACAL_1          = 0x7428,  /**< Caracal-1 CE switch */
    VTSS_TARGET_CARACAL_2          = 0x7429,  /**< Caracal-2 CE switch */
    VTSS_TARGET_JAGUAR_1           = 0x7460,  /**< Jaguar-1 CE switch */
    VTSS_TARGET_LYNX_1             = 0x7462,  /**< LynX-1 CE switch */
    VTSS_TARGET_CE_MAX_24          = 0x7364,  /**< CE-MaX-24 CE MAC */
    VTSS_TARGET_CE_MAX_12          = 0x7366,  /**< CE-MaX-12 CE MAC */
    VTSS_TARGET_E_STAX_III_48      = 0x7432,  /**< E-StaX-III-48 */
    VTSS_TARGET_E_STAX_III_68      = 0x7434,  /**< E-StaX-III-68 */
    VTSS_TARGET_E_STAX_III_24_DUAL = 0xD7431, /**< Dual E-StaX-III-24 */
    VTSS_TARGET_E_STAX_III_68_DUAL = 0xD7434, /**< Dual E-StaX-III-68 */
    VTSS_TARGET_DAYTONA            = 0x8492,  /**< Daytona FEC OTN Phy */
    VTSS_TARGET_TALLADEGA          = 0x8494,  /**< Talladega FEC OTN Phy */
    VTSS_TARGET_SERVAL_2           = 0x7438,  /**< Serval-2 CE switch */
    VTSS_TARGET_LYNX_2             = 0x7464,  /**< LynX-2 CE switch */
    VTSS_TARGET_JAGUAR_2           = 0x7468,  /**< Jaguar-2 CE switch */
} vtss_target_type_t;

/** \brief Create structure */
typedef struct {
    vtss_target_type_t target; /**< Target type */
} vtss_inst_create_t;

/** 
 * \brief Initialize create structure for target. 
 *
 * \param target [IN] Target name
 * \param create [IN] Create structure
 *
 * \return Return code.
 **/
vtss_rc vtss_inst_get(const vtss_target_type_t target,
                      vtss_inst_create_t       *const create);

/** 
 * \brief Create target instance.
 * 
 * \param create [IN] Create structure
 * \param inst [OUT]  Target instance reference.
 *
 * \return Return code.
 **/
vtss_rc vtss_inst_create(const vtss_inst_create_t *const create,
                         vtss_inst_t              *const inst);

/**
 * \brief Destroy target instance.
 * 
 * \param inst [IN] Target instance reference.
 *
 * \return Return code.
 **/
vtss_rc vtss_inst_destroy(const vtss_inst_t inst);

/** 
 * \brief Register read function 
 *
 * \param chip_no [IN] Chip number, for targets with multiple chips
 * \param addr [IN]    Register address
 * \param value [OUT]  Register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_reg_read_t)(const vtss_chip_no_t chip_no,
                                   const u32            addr,
                                   u32                  *const value);

/** 
 * \brief Register write function 
 *
 * \param chip_no [IN] Chip number, for targets with multiple chips
 * \param addr [IN]    Register address
 * \param value [IN]   Register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_reg_write_t)(const vtss_chip_no_t chip_no,
                                  const u32            addr,
                                  const u32            value);

/** 
 * \brief I2C read function 
 *
 * \param port_no [IN] Port number
 * \param i2c_addr [IN] I2C device address
 * \param addr [IN]   Register address
 * \param data [OUT]  Pointer the register(s) data value.
 * \param cnt [IN]    Number of registers to read
 * \param i2c_clk_sel [IN] If i2c clock multiplexing is supported then this is the i2c mux, else use NO_I2C_MULTIPLEXER
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_i2c_read_t)(const vtss_port_no_t port_no, 
                                   const u8 i2c_addr, 
                                   const u8 addr, 
                                   u8 *const data, 
                                   const u8 cnt,
                                   const i8 i2c_clk_sel);

/** 
 * \brief I2C write function 
 *
 * \param port_no [IN] Port number
 * \param i2c_addr [IN] I2C device address
 * \param data [OUT]  Pointer the data to be written.
 * \param cnt [IN]    Number of data bytes to write
 * \param i2c_clk_sel [IN] If i2c clock multiplexing is supported then this is the i2c mux, else use NO_I2C_MULTIPLEXER
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_i2c_write_t)(const vtss_port_no_t port_no, 
                                    const u8 i2c_addr, 
                                    u8 *const data, 
                                    const u8 cnt,
                                    const i8 i2c_clk_sel);

/**
 * \brief SPI read/write function
 *
 * \param inst [IN] Vitesse API instance.
 * \param port_no [IN] Port number.
 * \param bitsize [IN] Size (in bytes) of bitstream following this parameter.
 * \param data [IN|OUT] Pointer to the data to be written to SPI Slave, if doing write operation.
 *                      Pointer to the data read from SPI Slave, if doing read operation.
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_spi_read_write_t)(const vtss_inst_t inst,
                                       const vtss_port_no_t port_no,
                                       const u8 bitsize,
                                       u8 *const bitstream);


/** 
 * \brief MII management read function (IEEE 802.3 clause 22)
 *
 * \param inst [IN]    Target instance reference.
 * \param port_no [IN] Port number
 * \param addr [IN]    Register address (0-31)
 * \param value [OUT]  Register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_miim_read_t)(const vtss_inst_t    inst,
                                    const vtss_port_no_t port_no,
                                    const u8             addr,
                                    u16                  *const value);

/** 
 * \brief MII management write function (IEEE 802.3 clause 22)
 *
 * \param inst [IN]    Target instance reference.
 * \param port_no [IN] Port number
 * \param addr [IN]    Register address (0-31)
 * \param value [IN]   Register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_miim_write_t)(const vtss_inst_t    inst,
                                     const vtss_port_no_t port_no,
                                     const u8             addr,
                                     const u16            value);

/** 
 * \brief MMD management read function (IEEE 802.3 clause 45)
 *
 * \param inst [IN]    Target instance reference.
 * \param port_no [IN] Port number
 * \param mmd [IN]     MMD address (0-31)
 * \param addr [IN]    Register address (0-65535)
 * \param value [OUT]  Register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_mmd_read_t)(const vtss_inst_t    inst,
                                   const vtss_port_no_t port_no,
                                   const u8             mmd,
                                   const u16            addr,
                                   u16                  *const value);

/** 
 * \brief MMD management read increment function (IEEE 802.3 clause 45)
 *
 * \param inst [IN]    Target instance reference.
 * \param port_no [IN] Port number
 * \param mmd [IN]     MMD address (0-31)
 * \param addr [IN]    Start register address (0-65535)
 * \param buf [OUT]    The register values (pointer provided by user)
 * \param count [IN]   Number of register reads (increment register reads)
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_mmd_read_inc_t)(const vtss_inst_t    inst,
                                       const vtss_port_no_t port_no,
                                       const u8             mmd,
                                       const u16            addr,
                                       u16                  *const buf,
                                       u8                   count);

/** 
 * \brief MMD management write function (IEEE 802.3 clause 45)
 *
 * \param inst [IN]    Target instance reference.
 * \param port_no [IN] Port number
 * \param mmd [IN]     MMD address (0-31)
 * \param addr [IN]    Start register address (0-65535)
 * \param buf [IN]     The register value
 *
 * \return Return code.
 **/
typedef vtss_rc (*vtss_mmd_write_t)(const vtss_inst_t    inst,
                                    const vtss_port_no_t port_no,
                                    const u8             mmd,
                                    const u16            addr,
                                    const u16            value);

/** \brief PI data width */
typedef enum {
    VTSS_PI_WIDTH_16 = 0, /* 16 bit (default) */
    VTSS_PI_WIDTH_8       /* 8 bit */
} vtss_pi_width_t;

/** \brief PI configuration */
typedef struct {
    u32             cs_wait_ns;             /**< Minimum CS wait time in nanoseconds */
} vtss_pi_conf_t;

/** \brief Restart information source */
typedef enum {
    VTSS_RESTART_INFO_SRC_NONE,   /* Restart information not stored in PHYs */
    VTSS_RESTART_INFO_SRC_CU_PHY, /* Restart information stored in 1G PHY */
    VTSS_RESTART_INFO_SRC_10G_PHY /* Restart information stored in 10G PHY */
} vtss_restart_info_src_t;

/** \brief Initialization configuration. */
typedef struct {
    /* Register access function are not used for VTSS_TARGET_CU_PHY */
    vtss_reg_read_t   reg_read;     /**< Register read function */
    vtss_reg_write_t  reg_write;    /**< Register write function */

    /* MII management access must be setup for VTSS_TARGET_CU_PHY */
    vtss_miim_read_t  miim_read;    /**< MII management read function */
    vtss_miim_write_t miim_write;   /**< MII management write function */ 

    /* MMD management access must be setup for 10G phys */
    vtss_mmd_read_t     mmd_read;     /**< MMD management read function */
    vtss_mmd_read_inc_t mmd_read_inc; /**< MMD management read increment function */
    vtss_mmd_write_t    mmd_write;    /**< MMD management write function */

    vtss_spi_read_write_t   spi_read_write;/**< Board specific SPI read/write callout function */

    BOOL                    warm_start_enable; /**< Allow warm start */
    vtss_restart_info_src_t restart_info_src;  /**< Source of restart information */
    vtss_port_no_t          restart_info_port; /**< Port used to store PHY restart information */

    vtss_pi_conf_t    pi;           /**< Parallel Interface configuration */

} vtss_init_conf_t;


/** 
 * \brief Get default initialization configuration
 *
 * \param inst [IN]  Target instance reference
 * \param conf [OUT] Initialization configuration
 *
 * \return Return code.
 **/
vtss_rc vtss_init_conf_get(const vtss_inst_t inst,
                           vtss_init_conf_t  *const conf);

/** 
 * \brief Set initialization configuration.
 *
 * \param inst [IN] Target instance reference
 * \param conf [IN] Initialization configuration
 *
 * \return Return code.
 **/
vtss_rc vtss_init_conf_set(const vtss_inst_t      inst,
                           const vtss_init_conf_t *const conf);

/** 
 * \brief Indicate configuration end. 
 * If a warm start has been done, the stored configuration will be applied.
 *
 * \param inst [IN] Target instance reference
 *
 * \return Return code.
 **/
vtss_rc vtss_restart_conf_end(const vtss_inst_t inst);

/** \brief API version */
typedef u16 vtss_version_t;

/** \brief Restart type */
typedef enum {
    VTSS_RESTART_COLD, /**< Cold: Chip and CPU restart, e.g. power cycling */
    VTSS_RESTART_COOL, /**< Cool: Chip and CPU restart done by CPU */
    VTSS_RESTART_WARM  /**< Warm: CPU restart only */
} vtss_restart_t;

/** \brief Restart status */
typedef struct {
    vtss_restart_t restart;      /**< Previous restart mode */
    vtss_version_t prev_version; /**< Previous API version */
    vtss_version_t cur_version;  /**< Current API version */
} vtss_restart_status_t;

/** 
 * \brief Get restart status
 *
 * \param inst [IN]    Target instance reference
 * \param status [OUT] Restart status
 *
 * \return Return code.
 **/
vtss_rc vtss_restart_status_get(const vtss_inst_t inst,
                                vtss_restart_status_t *const status);

/** 
 * \brief Get restart configuration (next restart mode)
 *
 * \param inst [IN]     Target instance reference
 * \param restart [OUT] Restart mode
 *
 * \return Return code.
 **/
vtss_rc vtss_restart_conf_get(const vtss_inst_t inst,
                              vtss_restart_t *const restart);

/** 
 * \brief Set restart configuration (next restart mode)
 *
 * \param inst [IN]    Target instance reference
 * \param restart [IN] Restart mode
 *
 * \return Return code.
 **/
vtss_rc vtss_restart_conf_set(const vtss_inst_t inst,
                              const vtss_restart_t restart);


#ifdef __cplusplus
}
#endif
#endif /* _VTSS_INIT_API_H_ */
