/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.
 
 $Id$
 $Revision$

*/
/**
 * \file
 * \brief OS Layer API
 * \details This header file includes the OS specific header file
 */

#ifndef _VTSS_OS_H_
#define _VTSS_OS_H_

 #include <vtss_os_linux.h>

/*
 * Don't add default VTSS_xxx() macro implementations here,
 * since that might lead to uncaught problems on new platforms.
 */

#endif /* _VTSS_OS_H_ */
