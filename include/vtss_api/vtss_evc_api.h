/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.
 
 $Id$
 $Revision$

*/

/**
 * \file
 * \brief EVC API
 * \details This header file describes EVC functions
 */

#ifndef _VTSS_EVC_API_H_
#define _VTSS_EVC_API_H_

#include <vtss/api/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VTSS_EVC_POLICERS           64   /**< Maximum number of EVC policers */

#define VTSS_EVC_POLICER_ID_DISCARD 4094 /**< EVC/ECE: Policer discards all frames */
#define VTSS_EVC_POLICER_ID_NONE    4095 /**< EVC/ECE: Policer forwards all frames */
#define VTSS_EVC_POLICER_ID_EVC     4096 /**< ECE only: Use EVC policer */

/** \brief EVC policer configuration */
typedef vtss_dlb_policer_conf_t vtss_evc_policer_conf_t;

/**
 * \brief Get EVC policer configuration.
 *
 * \param inst [IN]        Target instance reference.
 * \param policer_id [IN]  Policer ID.
 * \param conf [OUT]       Policer configuration.
 *
 * \return Return code.
 **/
vtss_rc vtss_evc_policer_conf_get(const vtss_inst_t           inst,
                                  const vtss_evc_policer_id_t policer_id,
                                  vtss_evc_policer_conf_t     *const conf);

/**
 * \brief Set EVC policer configuration.
 *
 * \param inst [IN]        Target instance reference.
 * \param policer_id [IN]  Policer ID.
 * \param conf [IN]        Policer configuration.
 *
 * \return Return code.
 **/
vtss_rc vtss_evc_policer_conf_set(const vtss_inst_t             inst,
                                  const vtss_evc_policer_id_t   policer_id,
                                  const vtss_evc_policer_conf_t *const conf);

#ifdef __cplusplus
}
#endif
#endif /* _VTSS_EVC_API_H_ */
