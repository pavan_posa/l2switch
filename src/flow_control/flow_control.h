/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * flow_control.h
 */

#ifndef FLOW_CONTROL_H_
#define FLOW_CONTROL_H_

#include <fsl_utils/fsl_utils.h>

/* flow control operations */
int port_flow_control_autoneg_set(int port_no, BOOL aneg);
int port_flow_control_autoneg_get(int port_no, BOOL *aneg);
int port_flow_control_set(int port_no, vtss_port_flow_control_conf_t *fc_conf);
int port_flow_control_get(int port_no, vtss_port_flow_control_conf_t *fc_conf);
void port_flow_control_show(int port_no);
void port_flow_control_show_all(void);

#endif /* FLOW_CONTROL_H_ */
