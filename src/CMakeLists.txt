#
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
#

add_subdirectory (acl)
add_subdirectory (board_init)
add_subdirectory (cli)
add_subdirectory (control_traffic)
add_subdirectory (flow_control)
add_subdirectory (fsl_utils)
add_subdirectory (lag)
add_subdirectory (mac)
add_subdirectory (mirror)
add_subdirectory (multicast)
add_subdirectory (port_counters)
add_subdirectory (port_filters)
add_subdirectory (port_isolation)
add_subdirectory (port_setup)
add_subdirectory (qos)
add_subdirectory (vlan)
add_subdirectory (main)
