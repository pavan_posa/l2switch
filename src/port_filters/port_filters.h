/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _PORT_FILTERS_H_
#define _PORT_FILTERS_H_

#include <fsl_utils/fsl_utils.h>

int set_max_frame_length(int port_no, int max_frame_length);
int set_unicast_flood_member(bool enable, unsigned int port_no);
int set_port_learning_mode(unsigned int port_no, unsigned int mode);

int unicast_command(const char *command);
int learn_command(const char *command);

/* commands for VLAN tag filter */
int port_vlan_tag_filter_set(int port_no, vtss_vlan_frame_t allow_frame_tag);
void port_vlan_tag_filter_show(int port_no);
void port_vlan_tag_filter_show_all(void);

#endif /* _PORT_FILTERS_H_ */
