/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "port_filters.h"

int set_max_frame_length(int port_no, int max_frame_length)
{
	vtss_port_conf_t pconf;

	if (!is_valid_port(port_no)) {
		printf("ERROR:%s:%d: Invalid port number!",
				__FILE__, __LINE__);
		return -1;
	}

	if (vtss_port_conf_get(NULL, port_no, &pconf) != VTSS_RC_OK)
		return -1;
	pconf.max_frame_length = max_frame_length;
	if (vtss_port_conf_set(NULL, port_no, &pconf) != VTSS_RC_OK)
		return -1;

	return 0;
}

int set_unicast_flood_member(bool enable, unsigned int port_no)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (!is_valid_port(port_no)) {
		printf("ERROR:%s:%d: Invalid port number!",
				__FILE__, __LINE__);
		return -1;
	}

	if (vtss_uc_flood_members_get(NULL, member) != VTSS_RC_OK) {
		printf("ERROR:%s:%d: Could not get UC flood members!",
				__FILE__, __LINE__);
		return -1;
	}
	member[port_no] = enable;
	if (vtss_uc_flood_members_set(NULL, member) != VTSS_RC_OK) {
		printf("ERROR:%s:%d: Could not set UC flood members!",
				__FILE__, __LINE__);
		return -1;
	}

	return 0;
}

/* Set learning mode of the switch.
 * Current supported learning modes: off, auto
 */
int set_port_learning_mode(unsigned int port_no, unsigned int mode)
{
	vtss_learn_mode_t lmode;

	if (!is_valid_port(port_no)) {
		printf("ERROR:%s:%d: Invalid port number!",
				__FILE__, __LINE__);
		return -1;
	}

	switch (mode) {
	case (LEARN_MODE_OFF):
		lmode.discard = FALSE;
		lmode.cpu = FALSE;
		lmode.automatic = FALSE;
		break;
	case (LEARN_MODE_AUTO):
		lmode.discard = FALSE;
		lmode.cpu = FALSE;
		lmode.automatic = TRUE;
		break;
	default:
		printf("ERROR:%s:%d: Unknown learning mode!",
				__FILE__, __LINE__);
		return -1;
	}

	if (vtss_learn_port_mode_set(NULL, port_no, &lmode) != VTSS_RC_OK) {
		printf("ERROR:%s:%d: Could not set learning mode on port %u!",
				__FILE__, __LINE__, port_no);
		return -1;
	}

	return 0;
}

int unicast_command(const char *command)
{
	unsigned int port_no;

	if (strstr(command, "flood_member true port ") != NULL) {
		command += strlen("flood_member true port ");
		port_no = atoi(command);
		return set_unicast_flood_member(true, port_no);
	} else if (strstr(command, "flood_member false port ") != NULL) {
		command += strlen("flood_member false port ");
		port_no = atoi(command);
		return set_unicast_flood_member(false, port_no);
	} else {
		printf("ERROR:%s:%d: Unknown unicast parameters!",
				__FILE__, __LINE__);
		return -1;
	}
}

int learn_command(const char *command)
{
	unsigned int port_no;
	if (strstr(command, "off port ") != NULL) {
		command += strlen("off port ");
		port_no = atoi(command);
		return set_port_learning_mode(port_no, LEARN_MODE_OFF);
	} else if (strstr(command, "auto port ") != NULL) {
		command += strlen("auto port ");
		port_no = atoi(command);
		return set_port_learning_mode(port_no, LEARN_MODE_AUTO);
	} else {
		printf("ERROR:%s:%d: Unknown learn parameters!",
				__FILE__, __LINE__);
		return -1;
	}
}

/* set port filter for tag/untagged frames */
int port_vlan_tag_filter_set(const int port_no,
		vtss_vlan_frame_t allow_frame_tag)
{
	vtss_vlan_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return -1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("ERROR: Failed to get vlan port configuration\n");
		return -1;
	}

	conf.frame_type = allow_frame_tag;

	if (vtss_vlan_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("ERROR: Failed to set vlan port configuration\n");
		return -1;
	}

	return 0;
}

/* get port filter for tag/untagged frames */
static int port_vlan_tag_filter_get(const int port_no,
		vtss_vlan_frame_t *allow_frame_tag)
{
	vtss_vlan_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return -1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("ERROR: Failed to get vlan port configuration\n");
		return -1;
	}

	*allow_frame_tag = conf.frame_type;

	return 0;
}

/* show the VLAN tag filter for a port */
void port_vlan_tag_filter_show(const int port_no)
{
	vtss_vlan_frame_t allow_frame_tag;

	if (!is_valid_port(port_no))
		return;

	if (port_vlan_tag_filter_get(port_no, &allow_frame_tag)) {
		printf("ERROR: Failed to get vlan tag filter for port %d\n",
				port_no);
		return;
	}

	printf("Port %d\t allow ", port_no);
	switch(allow_frame_tag) {
	case VTSS_VLAN_FRAME_ALL:
		printf("all\n");
		break;
	case VTSS_VLAN_FRAME_TAGGED:
		printf("tagged only\n");
		break;
	case VTSS_VLAN_FRAME_UNTAGGED:
		printf("untagged only\n");
		break;
	default:
		printf("ERROR: Unknown tag filter %d\n", allow_frame_tag);
	}
}

/* show the VLAN tag filter for all ports */
void port_vlan_tag_filter_show_all(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		port_vlan_tag_filter_show(i);
}
