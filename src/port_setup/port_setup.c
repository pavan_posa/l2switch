/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "port_setup.h"

extern BOOL phy_present[];

/* ports are polled from time to
 * time to check for link status
 */
void port_polling(void)
{
	int			port_no;
	vtss_port_status_t	status_phy;
	vtss_port_conf_t	conf_port;
	BOOL			fc_aneg;
	BOOL			change;

	/* get current port configuration */
	for (port_no = VTSS_PORT_NO_START;
		port_no < VTSS_PORT_NO_END;
		port_no++)
		if (is_valid_port(port_no) && phy_present[port_no]) {
			change = FALSE;

			if (vtss_phy_status_get(NULL, port_no,
					&status_phy) != VTSS_RC_OK)
				printf("Can't get status for port's %d phy\n",
						port_no);

			/* if there is no link, no need to
			 * reconfigure the port
			 */
			if (status_phy.link == FALSE ||
					status_phy.link_down == TRUE)
				continue;

			if (vtss_port_conf_get(NULL, port_no,
					&conf_port) != VTSS_RC_OK)
				printf("Cannot get status for port %d\n",
					port_no);

			if (status_phy.speed > VTSS_SPEED_UNDEFINED &&
					status_phy.speed <= VTSS_SPEED_12G &&
					conf_port.speed != status_phy.speed) {
				change = TRUE;
				conf_port.speed = status_phy.speed;
			}

			if (conf_port.fdx != status_phy.fdx) {
				change = TRUE;
				conf_port.fdx = status_phy.fdx;
			}


			fc_aneg = FALSE;
			if (port_flow_control_autoneg_get(port_no,
						&fc_aneg) != 0)
				printf("Cannot get FC mode for port %d\n",
						port_no);

			if (fc_aneg == TRUE &&
				(conf_port.flow_control.generate !=
					status_phy.aneg.generate_pause ||
				conf_port.flow_control.obey !=
					status_phy.aneg.obey_pause)) {
				change = TRUE;
				conf_port.flow_control.generate = status_phy.aneg.generate_pause;
				conf_port.flow_control.obey = status_phy.aneg.obey_pause;
			}

			/*
			 * change port configuration only if
			 * something is changed
			 */
			if (change == FALSE)
				continue;

			if (vtss_port_conf_set(NULL, port_no,
						&conf_port) != VTSS_RC_OK)
				printf("Cannot set speed %d for port %d\n",
						status_phy.speed, port_no);
		}
}

/* Setup port based on configuration and auto negotiation result */
void port_setup_init(int port_no, BOOL aneg)
{
#ifdef VTSS_CHIP_CU_PHY
	vtss_phy_conf_t         phy;
	vtss_port_status_t      status;
#endif
#if defined(VTSS_FEATURE_PORT_CONTROL)
	vtss_port_conf_t conf;
	if (vtss_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK)
		bzero(&conf, sizeof(conf));

	if (is_internal_port(port_no))
		conf.if_type = VTSS_PORT_INTERFACE_VAUI;
	else
		conf.if_type = VTSS_PORT_INTERFACE_QSGMII;
	conf.power_down = FALSE;
	conf.flow_control.smac.addr[5] = port_no;
	conf.max_frame_length = VTSS_MAX_FRAME_LENGTH_MAX;
#endif /* VTSS_FEATURE_PORT_CONTROL */

	if (phy_present[port_no] == TRUE && aneg) {
#if defined(VTSS_FEATURE_PORT_CONTROL)
		/* Setup port based on auto negotiation status */
		vtss_phy_status_get(NULL, port_no, &status);
		conf.speed = status.speed;
		conf.fdx = status.fdx;
		conf.flow_control.obey = status.aneg.obey_pause;
		conf.flow_control.generate = status.aneg.generate_pause;
#endif /* VTSS_FEATURE_PORT_CONTROL */
	} else {

		/* Setup port based on configuration */
#ifdef VTSS_CHIP_CU_PHY
		if (phy_present[port_no] == FALSE)
			/* port has no PHY attached */
			goto port_conf;
		bzero(&phy, sizeof(phy));
		phy.mode = VTSS_PHY_MODE_ANEG;
		phy.aneg.speed_10m_hdx = TRUE;
		phy.aneg.speed_10m_fdx = TRUE;
		phy.aneg.speed_100m_hdx = TRUE;
		phy.aneg.speed_100m_fdx = TRUE;
		phy.aneg.speed_1g_fdx = TRUE;
		phy.aneg.symmetric_pause = FALSE;
		phy.aneg.asymmetric_pause = FALSE;
		phy.forced.speed = VTSS_SPEED_1G;
		phy.forced.fdx = TRUE;
		phy.mdi = VTSS_PHY_MDIX_AUTO;

		vtss_phy_conf_set(NULL, port_no, &phy);
port_conf:
#endif

#if defined(VTSS_FEATURE_PORT_CONTROL)
		/* Use configured values */
		if (is_internal_port(port_no))
			conf.speed = VTSS_SPEED_2500M;
		else
			conf.speed = VTSS_SPEED_1G;
		conf.fdx = TRUE;
		conf.flow_control.obey = FALSE;
		conf.flow_control.generate = FALSE;
#endif /* VTSS_FEATURE_PORT_CONTROL */
	}

	/* by default, flow control is disabled
	 * and not based on autonegotiation */
	if (port_flow_control_autoneg_set(port_no, FALSE) != 0)
		printf("Cannot set flow control mode for port %d\n", port_no);
#if defined(VTSS_FEATURE_PORT_CONTROL)
	vtss_port_conf_set(NULL, port_no, &conf);
#endif /* VTSS_FEATURE_PORT_CONTROL */
}

/* set link speed for a port */
int port_config_set(int port_no, int speed, int duplexity)
{
	vtss_port_conf_t	conf;
	vtss_phy_conf_t		phy_conf;

	if (!is_valid_port(port_no))
		return 1;

	/* get current configuration */
	if (vtss_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Cannot get configuration for port %d\n", port_no);
		return 1;
	}

	switch (speed) {
	case 10:
		conf.speed = VTSS_SPEED_10M; break;
	case 100:
		conf.speed = VTSS_SPEED_100M; break;
	case 1000:
		conf.speed = VTSS_SPEED_1G; break;
	case 2500:
		conf.speed = VTSS_SPEED_2500M; break;
	case 5000:
		conf.speed = VTSS_SPEED_5G; break;
	case 10000:
		conf.speed = VTSS_SPEED_10G; break;
	case 12000:
		conf.speed = VTSS_SPEED_12G; break;
	default:
		return 1;
	}

	if (duplexity != -1)
		conf.fdx = duplexity;

	/* set port conf*/
	if (vtss_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Cannot set speed %d for port %d\n", speed, port_no);
		return 1;
	}

	if (phy_present[port_no] == FALSE)
		return 0;

	/* get current configuration */
	if (vtss_phy_conf_get(NULL, port_no, &phy_conf) != VTSS_RC_OK) {
		printf("Cannot get phy configuration for port %d\n", port_no);
		return 1;
	}

	/* 1G link speed is possible only with autonegotiation */
	if (conf.speed > VTSS_SPEED_100M)
		phy_conf.mode = VTSS_PHY_MODE_ANEG;
	else
		phy_conf.mode = VTSS_PHY_MODE_FORCED;

	phy_conf.forced.speed = conf.speed;
	phy_conf.forced.fdx = conf.fdx;

	/* set configuration */
	if (vtss_phy_conf_set(NULL, port_no, &phy_conf) != VTSS_RC_OK) {
		printf("Cannot set autonegociation for port %d\n", port_no);
		return 1;
	}

	return 0;
}

/* print port configuration */
void port_config_show(int port_no)
{
	vtss_port_status_t	port_status;
	vtss_phy_conf_t		phy_conf;
	vtss_port_conf_t	port_conf;
	BOOL			state;

	if (!is_valid_port(port_no))
		return;

	/* get current configuration */
	if (!is_internal_port(port_no)) {
		if (vtss_port_status_get(NULL, port_no, &port_status) !=
				VTSS_RC_OK) {
			printf("Cannot get status for port %d\n", port_no);
			return;
		}
	} else if (vtss_port_conf_get(NULL, port_no, &port_conf) !=
			VTSS_RC_OK) {
		printf("Cannot get config for port %d\n", port_no);
		return;
	}

	if (phy_present[port_no])
		if (vtss_phy_conf_get(NULL, port_no, &phy_conf) != VTSS_RC_OK) {
			printf("Cannot get phy config for port %d\n", port_no);
			return;
		}

	if (vtss_port_state_get(NULL, port_no, &state) != VTSS_RC_OK) {
		printf("Cannot get state for port %d\n", port_no);
		return;
	}

	printf("Port %d\t", port_no);
	state == TRUE ?
			printf("state: enabled\t") :
			printf("state: disabled\t");
	if (!is_internal_port(port_no)) {
		port_status.link == TRUE ?
				printf("link: up\t") :
				printf("link: down\t");

		if (phy_present[port_no]) {
			printf("mode: ");
			switch (phy_conf.mode) {
			case VTSS_PHY_MODE_ANEG:
				printf("auto\t"); break;
			case VTSS_PHY_MODE_FORCED:
				printf("forced\t"); break;
			case VTSS_PHY_MODE_POWER_DOWN:
			default:
				printf("power down\t");
			}
		}

		printf("speed: ");
		switch (port_status.speed) {
		case VTSS_SPEED_10M:
			printf("10M\t"); break;
		case VTSS_SPEED_100M:
			printf("100M\t"); break;
		case VTSS_SPEED_1G:
			printf("1G\t"); break;
		case VTSS_SPEED_2500M:
			printf("2.5G\t"); break;
		case VTSS_SPEED_5G:
			printf("5G\t"); break;
		case VTSS_SPEED_10G:
			printf("10G\t"); break;
		case VTSS_SPEED_12G:
			printf("12G\t"); break;
		case VTSS_SPEED_UNDEFINED:
		default:
			printf("???\t");
		}

		port_status.fdx == TRUE ?
				printf("duplex: full\n") :
				printf("duplex: half\n");
	} else {
		/* for internal ports */

		printf("link: down\t");
		printf("mode: static\t");

		printf("speed: ");
		switch (port_conf.speed) {
		case VTSS_SPEED_10M:
			printf("10M\t"); break;
		case VTSS_SPEED_100M:
			printf("100M\t"); break;
		case VTSS_SPEED_1G:
			printf("1G\t"); break;
		case VTSS_SPEED_2500M:
			printf("2.5G\t"); break;
		case VTSS_SPEED_5G:
			printf("5G\t"); break;
		case VTSS_SPEED_10G:
			printf("10G\t"); break;
		case VTSS_SPEED_12G:
			printf("12G\t"); break;
		case VTSS_SPEED_UNDEFINED:
		default:
			printf("???\t");
		}

		port_conf.fdx == TRUE ?
				printf("duplex: full\n") :
				printf("duplex: half\n");
	}
}

/* print all port configurations */
void port_config_show_all(void)
{
	int port_no;

	for (port_no = VTSS_PORT_NO_START;
			port_no < VTSS_PORT_NO_END;
			port_no++)
			port_config_show(port_no);
}

/* enable/disable autonegotiation for a port */
int port_autoneg_set(int port_no, BOOL aneg)
{
	vtss_phy_conf_t      conf;

	if (!is_valid_port(port_no))
		return 1;

	if (!phy_present[port_no])
		return 1;

	/* get current configuration */
	if (vtss_phy_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Cannot get configuration for port %d\n", port_no);
		return 1;
	}

	if (aneg == TRUE)
		conf.mode = VTSS_PHY_MODE_ANEG;
	else {
		conf.mode = VTSS_PHY_MODE_FORCED;
		conf.forced.speed = VTSS_SPEED_1G;
		conf.forced.fdx = TRUE;
	}

	/* set autonegotiation */
	if (vtss_phy_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Cannot set autonegotiation %d for port %d\n",
				aneg,
				port_no);
		return 1;
	}
	return 0;
}
