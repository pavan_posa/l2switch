/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "mac.h"

/* Use <MAC_ALL_VLANS> as an VLAN ID to make the function ignore VLANs */
#define MAC_ALL_VLANS		4097

#define MAC_LEN_STR		"17"
#define PORT_LEN_STR		"2"
#define VID_LEN_STR		"4"

/*
 * ============================================================================
 * HELPER FUNCTIONS
 * ============================================================================
 */

/*
 * Compare two mac_vid structures
 */
static BOOL mac_vid_compare(vtss_vid_mac_t *entry1, vtss_vid_mac_t *entry2)
{
	if (entry1->vid != MAC_ALL_VLANS && entry2->vid != MAC_ALL_VLANS) {
		if (entry1->vid != entry2->vid)
			return 0;
	}

	return memcmp(entry1->mac.addr, entry2->mac.addr, MAC_ADDR_COUNT) == 0;
}

/*
 * Print MAC table header
 */
static void print_mac_table_header(void)
{
	/* print table header */
	printf("Type    VID  MAC Address        Ports\n");
	printf("------  ---  -----------------  -----\n");
}

/*
 * Print information for a MAC entry
 */
static void print_mac_entry(vtss_mac_table_entry_t mac_entry)
{
	int i;

	printf("%s %-4d "MAC_ADDR_PRINT_FORMAT"  ",
			mac_entry.locked ? "Static " : "Dynamic",
			mac_entry.vid_mac.vid,
			MAC_ADDR_PRINT_ARGS(mac_entry.vid_mac.mac.addr));

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i) &&
		    mac_entry.destination[i])
			printf("%d ", i);
	printf("\n");
}

/*
 * ============================================================================
 * MAC TABLE COMMANDS IMPLEMENTATION
 * ============================================================================
 */

/*
 * Add a static entry in the MAC table by specifying MAC address,
 * port (and VLAN ID)
 */
static int mac_add_cmd(const char *command)
{
	u8	addr[6];
	int	port, vid = VTSS_VID_DEFAULT;
	char	*endptr;
	char	mac_str[MAC_ADDR_STR_LENGTH], port_str[2], vid_str[4];

	vtss_mac_table_entry_t	entry;
	vtss_rc			rc;

	memset(&entry, 0, sizeof(vtss_mac_table_entry_t));

	/* parse parameters */
	if (sscanf(command, "%"MAC_LEN_STR"s %"PORT_LEN_STR"s %"VID_LEN_STR"s",
			mac_str, port_str, vid_str) == EOF) {
		perror("Parsing error");
		return -1;
	}

	if (parse_mac(mac_str, addr) != MAC_ADDR_COUNT) {
		printf("ERROR:Invalid MAC address %s\n", mac_str);
		return -1;
	}

	if (strlen(port_str) == 0)
		return -1;

	port = strtol(port_str, &endptr, 10);
	if (port_str ==  endptr || !is_valid_port(port)) {
		printf("ERROR:Invalid port specified\n");
		return -1;
	}

	vid = strtol(vid_str, &endptr, 10);
	if (vid_str == endptr)
		vid = VTSS_VID_DEFAULT;

	if (!is_valid_vid(vid)) {
		printf("ERROR:Invalid VID specified\n");
		return -1;
	}

	/* add entry */
	memcpy(&entry.vid_mac.mac, addr, MAC_ADDR_COUNT);
	entry.vid_mac.vid = vid;
	entry.locked = 1; /* static */
	entry.destination[port] = TRUE;

	rc = vtss_mac_table_add(NULL, &entry);
	if (rc != VTSS_RC_OK) {
		printf("Error calling vtss_mac_table_add (%d)", rc);
		return -1;
	}

	return 0;
}

/*
 * Delete entry with a specific MAC (and VLAN ID)
 */
static int mac_del_cmd(const char *command)
{
	u8	addr[6];
	int	vid = VTSS_VID_DEFAULT;
	char	*endptr;
	char	mac_str[MAC_ADDR_STR_LENGTH], vid_str[4];
	BOOL	all_vlans = FALSE;

	vtss_vid_mac_t		vid_mac, vid_mac_tmp;
	vtss_mac_table_entry_t	next_entry;
	vtss_rc			rc;

	/* parse parameters */
	if (sscanf(command, "%"MAC_LEN_STR"s %"VID_LEN_STR"s",
			mac_str, vid_str) == EOF) {
		perror("Parsing error");
		return -1;
	}

	if (parse_mac(mac_str, addr) != MAC_ADDR_COUNT) {
		printf("ERROR:Invalid MAC address <%s\n", mac_str);
		return -1;
	}

	if (!strlen(vid_str)) /* no VLAN ID specified; delete from all vlans*/
		all_vlans = TRUE;

	vid = strtol(vid_str, &endptr, 10);
	if (vid_str == endptr)
		vid = VTSS_VID_DEFAULT;
	if (!is_valid_vid(vid)) {
		printf("ERROR:Invalid VID specified\n");
		return -1;
	}

	memcpy(&vid_mac.mac.addr, addr, MAC_ADDR_COUNT);

	/* delete entry */
	if (all_vlans) {
		vid_mac.vid = MAC_ALL_VLANS; /* No vlan ID specified */

		memset(&vid_mac_tmp, 0, sizeof(vtss_vid_mac_t));
		memset(&next_entry, 0, sizeof(vtss_mac_table_entry_t));

		/* look into the MAC table */
		while (vtss_mac_table_get_next(NULL, &vid_mac_tmp, &next_entry)
						== VTSS_RC_OK) {
			/* delete if match on MAC addr */
			if (mac_vid_compare(&next_entry.vid_mac, &vid_mac)) {
				rc = vtss_mac_table_del(NULL,
						&next_entry.vid_mac);
				if (rc != VTSS_RC_OK) {
					printf("Error: vtss_mac_table_del (%d)",
							rc);
					return -1;
				}
			}

			/* search again from next entry */
			vid_mac_tmp = next_entry.vid_mac;
		}
	} else {
		vid_mac.vid = vid;
		rc = vtss_mac_table_del(NULL, &vid_mac);
		if (rc != VTSS_RC_OK) {
			printf("Invalid entry specified (%d)\n", rc);
			return 0; /* not necessarily an error */
		}
	}

	return 0;
}

/*
 * Lookup for entry with specific MAC address (and VLAN)
 */
static int mac_lookup_cmd(const char *command)
{
	u8	addr[6];
	int	vid = VTSS_VID_DEFAULT;
	char	*endptr;
	char	mac_str[MAC_ADDR_STR_LENGTH], vid_str[4];
	BOOL	first = TRUE;

	vtss_vid_mac_t		vid_mac, vid_mac_tmp;
	vtss_mac_table_entry_t	next_entry;

	/* parse parameters */
	if (sscanf(command, "%"MAC_LEN_STR"s %"VID_LEN_STR"s",
			mac_str, vid_str) == EOF) {
		perror("Parsing error");
		return -1;
	}

	if (parse_mac(mac_str, addr) != MAC_ADDR_COUNT) {
		printf("ERROR:Invalid MAC address <%s\n", mac_str);
		return -1;
	}

	vid = strtol(vid_str, &endptr, 10);
	if (vid_str == endptr)
		vid = MAC_ALL_VLANS; /* No vlan ID specified */
	else if (!is_valid_vid(vid)) {
		printf("ERROR:Invalid VID specified\n");
		return -1;
	}

	/* lookup entry */
	memcpy(&vid_mac.mac.addr, addr, MAC_ADDR_COUNT);
	vid_mac.vid = vid;

	if (vid == MAC_ALL_VLANS) {
		/* ignore VLANs */
		memset(&vid_mac_tmp, 0, sizeof(vtss_vid_mac_t));
		memset(&next_entry, 0, sizeof(vtss_mac_table_entry_t));

		while (vtss_mac_table_get_next(NULL, &vid_mac_tmp, &next_entry)
				== VTSS_RC_OK) {
			if (mac_vid_compare(&next_entry.vid_mac, &vid_mac)) {
				if (first) {
					print_mac_table_header();
					first = FALSE;
				}

				print_mac_entry(next_entry);
			}
			/* search again from next address */
			vid_mac_tmp = next_entry.vid_mac;
		}
	} else {
		/* direct search */
		if (vtss_mac_table_get(NULL, &vid_mac_tmp, &next_entry)
				== VTSS_RC_OK) {
			print_mac_table_header();
			first = FALSE;
			print_mac_entry(next_entry);
		}
	}

	if (first) /* No entry found */
		printf("No entries found\n");

	return 0;
}

/*
 * Dump MAC table content
 */
static int mac_dump_cmd(void)
{
	ulong			static_count = 0, dynamic_count = 0;
	vtss_vid_mac_t		vid_mac_tmp;
	vtss_mac_table_entry_t	next_entry;

	memset(&vid_mac_tmp, 0 , sizeof(vtss_vid_mac_t));
	memset(&next_entry, 0, sizeof(vtss_mac_table_entry_t));

	print_mac_table_header();

	/* walk through the table */
	while (vtss_mac_table_get_next(NULL, &vid_mac_tmp, &next_entry)
			== VTSS_RC_OK) {
		print_mac_entry(next_entry);

		if (next_entry.locked)
			static_count++;
		else
			dynamic_count++;

		/* search again from next address */
		vid_mac_tmp = next_entry.vid_mac;
		memset(&next_entry, 0, sizeof(vtss_mac_table_entry_t));
	}

	printf("\nStatic  entries: %ld\n", static_count);
	printf("Dynamic entries: %ld\n", dynamic_count);
	return 0;
}

/*
 * Flush dynamic entries in the MAC table by port, by VLAN ID,
 * by port and VLAN ID or all.
 */
static int mac_flush_cmd(const char *command)
{
	BOOL	port_flush = FALSE, vid_flush = FALSE;
	int	port = VTSS_PORT_NO_START, vid = VTSS_VID_DEFAULT;
	char	*foundptr = NULL, *tmpptr = NULL;
	vtss_rc	rc;

	/* parse parameters */
	foundptr = strstr(command, "port ");
	if (foundptr) {
		foundptr += strlen("port ");
		port = strtol(foundptr, &tmpptr, 10);
		if (!is_valid_port(port))
			return -1;

		port_flush = TRUE;
	}

	foundptr = strstr(command, "vlan ");
	if (foundptr) {
		foundptr += strlen("vlan ");
		vid = strtol(foundptr, &tmpptr, 10);
		if (foundptr == tmpptr || !is_valid_vid(vid))
			return -1;
		vid_flush = TRUE;
	}

	if (strlen(command) > 1 && !(port_flush  || vid_flush)) {
		/* garbage in command line */
		return -1;
	}

	/* flush */
	if (port_flush && vid_flush)
		rc = vtss_mac_table_vlan_port_flush(NULL, port, vid);
	else if (port_flush && !vid_flush)
		rc = vtss_mac_table_port_flush(NULL, port);
	else if (!port_flush && vid_flush)
		rc = vtss_mac_table_vlan_flush(NULL, vid);
	else /*(!port_flush && !vid_flush) */
		rc = vtss_mac_table_flush(NULL);

	if (rc != VTSS_RC_OK) {
		printf("Error calling vtss_mac_table_*_flush (%d)", rc);
		return -1;
	}
	return 0;
}

/*
 * Handle getting/setting MAC age time value.
 * 0 disables dynamic entries aging.
 */
static int mac_agetime_cmd(const char *command)
{
	vtss_mac_table_age_time_t	agetime;
	vtss_rc				rc;

	if (strlen(command)) {
		agetime = atoi(command);
		rc = vtss_mac_table_age_time_set(NULL, agetime);
		if (rc == VTSS_RC_OK) {
			printf("MAC age time set to %d\n", agetime);
		} else {
			printf("Error calling vtss_mac_table_age_time_set (%d)",
					rc);
			return -1;
		}
	} else {
		rc = vtss_mac_table_age_time_get(NULL, &agetime);
		if (rc == VTSS_RC_OK) {
			printf("MAC age time: %d\n", agetime);
		} else {
			printf("Error calling vtss_mac_table_age_time_get (%d)",
					rc);
			return -1;
		}
	}
	return 0;
}

static int mac_learn_cmd(const char *command)
{
	int			port_no;
	vtss_learn_mode_t	mode;
	vtss_rc			rc;

	memset(&mode, 0, sizeof(mode));

	if (strncmp(command, "auto", strlen("auto")) == 0) {
		mode.automatic	= TRUE;
		mode.cpu	= FALSE;
		mode.discard	= FALSE;
	} else if (strncmp(command, "cpu", strlen("cpu")) == 0) {
		mode.automatic	= FALSE;
		mode.cpu	= TRUE;
		mode.discard	= FALSE;
	} else if (strncmp(command, "off", strlen("off")) == 0) {
		mode.automatic	= FALSE;
		mode.cpu	= FALSE;
		mode.discard	= FALSE;
	} else if (strncmp(command, "secure", strlen("secure")) == 0) {
		mode.automatic	= FALSE;
		mode.cpu	= TRUE;
		mode.discard	= TRUE;
	} else
		return -1;

	for (port_no = VTSS_PORT_NO_START;
			port_no < VTSS_PORT_NO_END; port_no++) {
		rc = vtss_learn_port_mode_set(NULL, port_no, &mode);
		if (rc != VTSS_RC_OK) {
			printf("Error: vtss_learn_port_mode_set(%d) (%d)",
					port_no, rc);
			/*TODO: if this happens we have inconsistencies across
			 * ports - fix this
			 */
		}
	}
	return 0;
}

/*
 * Process 'mac' command parameters
 * Command options:
 * add <mac> <port> [<vid>]		- Add a static entry
 * del <mac> [<vid>]			- Delete an entry
 * lookup <mac> [<vid>]			- Lookup specific entry
 * dump					- Dump table content
 * flush [port <port>] [vlan <vid>]	- Flush dynamic entries
 * agetime [<age_time>]			- Change aging timer
 * learn [auto|cpu|off|secure]		- Change learn mode
 */
int mac_command(const char *command)
{
	int incorrect_command = 1;

	if (strncmp(command, "add ", strlen("add ")) == 0) {
		command += strlen("add ");
		incorrect_command = mac_add_cmd(command);
	} else if (strncmp(command, "del ", strlen("del ")) == 0) {
		command += strlen("del ");
		incorrect_command = mac_del_cmd(command);
	} else if (strncmp(command, "lookup ", strlen("lookup ")) == 0) {
		command += strlen("lookup ");
		incorrect_command = mac_lookup_cmd(command);
	} else if (strncmp(command, "dump", strlen("dump")) == 0) {
		command += strlen("dump");
		incorrect_command = mac_dump_cmd();
	} else if (strncmp(command, "flush", strlen("flush")) == 0) {
		command += strlen("flush");
		incorrect_command = mac_flush_cmd(command);
	} else if (strncmp(command, "agetime", strlen("agetime")) == 0) {
		command += strlen("agetime");
		incorrect_command = mac_agetime_cmd(command);
	} else if (strncmp(command, "learn ", strlen("learn ")) == 0) {
		command += strlen("learn ");
		incorrect_command = mac_learn_cmd(command);
	}

	if (incorrect_command) {
		printf("Error: Incorrect command\n");
		printf("Options:\n");
		printf("\tadd <mac> <port> [<vid>]\n");
		printf("\tdel <mac> [<vid>]\n");
		printf("\tlookup <mac> [<vid>]\n");
		printf("\tdump\n");
		printf("\tflush [port <port>] [vlan <vid>]\n");
		printf("\tagetime [<age_time>]\n");
		printf("\tlearn [auto|cpu|off|secure]\n\n");
	}

	return incorrect_command ? -1 : 0;
}
