/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _FSL_UTILS_H_
#define _FSL_UTILS_H_

#include <stdbool.h>
#include <vtss_api.h>

#include <unistd.h>

#define DEBUG_LOG_TEMP "/var/log/l2switch_debug.log.XXXXXX"

#define ENABLED 1
#define DISABLED !ENABLED

#define LEARN_MODE_OFF		1
#define LEARN_MODE_AUTO		2

#define MAC_ADDR_SCAN_FORMAT \
	"%"SCNx8":%"SCNx8":%"SCNx8":%"SCNx8":%"SCNx8":%"SCNx8
#define MAC_ADDR_PRINT_FORMAT \
	"%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8
#define MAC_ADDR_SCAN_ARGS(ma) \
	&(ma)[0], &(ma)[1], &(ma)[2], &(ma)[3], &(ma)[4], &(ma)[5]
#define MAC_ADDR_PRINT_ARGS(ma) \
	(ma)[0], (ma)[1], (ma)[2], (ma)[3], (ma)[4], (ma)[5]
#define MAC_ADDR_COUNT 6
#define MAC_ADDR_STR_LENGTH 17

#define OUI_ADDR_SCAN_FORMAT \
	"%"SCNx8":%"SCNx8":%"SCNx8
#define OUI_ADDR_PRINT_FORMAT \
	"%02"PRIx8":%02"PRIx8":%02"PRIx8
#define OUI_ADDR_SCAN_ARGS(ma) \
	&(ma)[0], &(ma)[1], &(ma)[2]
#define OUI_ADDR_PRINT_ARGS(ma) \
	(ma)[0], (ma)[1], (ma)[2]
#define OUI_ADDR_COUNT MAC_ADDR_COUNT/2
#define OUI_ADDR_STR_LENGTH OUI_ADDR_COUNT * 3 - 1

/* Make the initialization/cleanup setup for l2switch PHYs */
void phy_init(void);
void phy_cleanup(void);

/* register read function */
vtss_rc l2sw_reg_read(const vtss_chip_no_t chip_no,
		const u32 addr,
		u32 *const value);

/* register write function */
vtss_rc l2sw_reg_write(const vtss_chip_no_t chip_no,
		const u32 addr,
		const u32 value);

/* MII management read function */
vtss_rc l2sw_miim_read(const vtss_inst_t inst,
		const vtss_port_no_t port_no,
		const u8 addr,
		u16 *const value);

/* MII management write function */
vtss_rc l2sw_miim_write(const vtss_inst_t    inst,
		const vtss_port_no_t port_no,
		const u8 addr,
		const u16 value);

/* find uio device */
bool uio_device_exists(char *path, size_t path_len);

/* Get next integer value from the string provided.
 * Returns the value as a parameter.
 * On exit, the string points after the parsed value.
 */
int get_next_value(const char **string, unsigned int *value);
int get_next_value_base(const char **string, unsigned int *value, int base);

/* Parse a string and get the IP as an u32 value */
int get_next_ip_address(const char **string, u32 *value);

/* Check if a port is also an internal port */
int is_internal_port(int port_no);

/* check port number
 * returns 0 if port is valid, 1 otherwise */
int is_valid_port(int port_no);

/* check VLAN value
 * returns 0 if VID is valid, 1 otherwise */
int is_valid_vid(int vid);

/* check Private VLAN value
 * returns 0 if PVLAN is valid, 1 otherwise */
int is_valid_private_vid(int pvlan);

/* parse MAC address from a string */
int parse_mac(const char *mac_string, u8 addr[6]);

/* parse OUI address from a string */
int parse_oui(const char *oui_string, u8 addr[]);

/* shows the current version of the demo app */
int show_version_command();

/*
 * Debug prototypes required by driver
 */

/* Trace call out function */
void vtss_callout_trace_printf(const vtss_trace_layer_t layer,
				const vtss_trace_group_t group,
				const vtss_trace_level_t level,
				const char *file,
				const int line,
				const char *function,
				const char *format,
				...);

/* Trace hex-dump call out function */

void vtss_callout_trace_hex_dump(const vtss_trace_layer_t layer,
				const vtss_trace_group_t group,
				const vtss_trace_level_t level,
				const char *file,
				const int line,
				const char *function,
				const unsigned char *byte_p,
				const int byte_cnt);

void vtss_callout_lock(const vtss_api_lock_t *const lock);
void vtss_callout_unlock(const vtss_api_lock_t *const lock);

#endif /* _FSL_UTILS_H_ */
