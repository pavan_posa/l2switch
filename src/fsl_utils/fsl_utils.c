/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "fsl_utils.h"

#include <stdio.h>
#include <string.h>
#include <linux/limits.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SYS_UIO_DIR		"/sys/class/uio"
#define SEVILLE_DEVNAME 	"Seville Switch"

#define PHY_PATH "/sys/bus/platform/devices/ffe800000.l2switch/"
#define INVALID_FD	-1

#define APP_VERSION	"0.4.0"

#define PHY_ID_VSC8514			0x00070670
#define PHY_ID_VSC8234			0x000FC623
#define PHY_ID_REG1			0x02
#define PHY_ID_REG2			0x03

volatile u32	*base_mem;

BOOL phy_present[VTSS_PORT_ARRAY_SIZE];
static int phy_reg[VTSS_PORT_ARRAY_SIZE];
static int phy_val[VTSS_PORT_ARRAY_SIZE];

struct {
	const char 	*device;
} phy_list[VTSS_PORT_ARRAY_SIZE] = {
	{ "phy_0", },
	{ "phy_1", },
	{ "phy_2", },
	{ "phy_3", },
	{ "phy_4", },
	{ "phy_5", },
	{ "phy_6", },
	{ "phy_7", },
	{NULL},
	{NULL},
};

int show_version_command()
{
	printf("L2 Switch demo app version %s\n", APP_VERSION);
	printf("Build Date: %s %s\n", __DATE__, __TIME__);
	printf("GIT Version: %s\n", GIT_VERSION);

	return 0;
}

static int open_mdio_reg(vtss_port_no_t port_no)
{
	char		fn[PATH_MAX];
	int		reg_fd = -1;

	if (port_no < VTSS_PORTS && phy_list[port_no].device)
	{
		snprintf(fn, sizeof(fn), "%s%s/phy_reg",
				PHY_PATH,
				phy_list[port_no].device);
		reg_fd = open(fn, O_RDWR | O_SYNC);
		if (reg_fd < 0)
		{
			printf("Error opening %s - %s\n", fn, strerror(errno));
			phy_list[port_no].device = NULL;
		}
	}
	return reg_fd;
}

static int open_mdio_val(vtss_port_no_t port_no)
{
	char		fn[PATH_MAX];
	int		val_fd = -1;

	if (port_no < VTSS_PORTS && phy_list[port_no].device)
	{
		snprintf(fn, sizeof(fn), "%s%s/phy_val",
				PHY_PATH,
				phy_list[port_no].device);
		val_fd = open(fn, O_RDWR | O_SYNC);
		if (val_fd < 0)
		{
			printf("Error opening %s - %s\n", fn, strerror(errno));
			phy_list[port_no].device = NULL;
		}
	}
	return val_fd;
}

static int read_val(int fd, u16 *value)
{
	char	rbuf[256];
	int	len;

	if (fd < 0)
		return -1;

	if (lseek(fd, 0, SEEK_SET) == (off_t)-1) {
		perror("Failed to reset cursor for sysfs file");
		return -1;
	}

	len = read(fd, rbuf, sizeof(rbuf)-1);
	if (len >= 0)
	{
		rbuf[len] = '\0';
		*value = (unsigned)atoi(rbuf);
	}

	return len;
}

static int write_val(int fd, u16 value)
{
	char	wbuf[256];
	int	len;

	if (fd < 0)
		return -1;

	if (lseek(fd, 0, SEEK_SET) == (off_t)-1) {
		perror("Failed to reset cursor for sysfs file");
		return -1;
	}

	len = snprintf(wbuf, sizeof(wbuf), "%u\n", value);
	if (len >= 0)
		write(fd, wbuf, len);

	return len;
}

vtss_rc l2sw_miim_read(const vtss_inst_t inst,
		const vtss_port_no_t port_no,
		const u8 addr,
		u16 *const value)
{
	/* Nothing to do if PHY is not present */
	if (phy_present[port_no] == FALSE)
		return VTSS_RC_ERROR;

	if (write_val(phy_reg[port_no], addr) < 0)
		return VTSS_RC_ERROR;

	if (read_val(phy_val[port_no], value) < 0)
		return VTSS_RC_ERROR;

	return VTSS_RC_OK;
}

vtss_rc l2sw_miim_write(const vtss_inst_t    inst,
		const vtss_port_no_t port_no,
		const u8 addr,
		const u16 value)
{
	/* Nothing to do if PHY is not present */
	if (phy_present[port_no] == FALSE)
		return VTSS_RC_ERROR;

	if (write_val(phy_reg[port_no], addr) < 0)
		return VTSS_RC_ERROR;

	if (write_val(phy_val[port_no], value) < 0)
		return VTSS_RC_ERROR;

	return VTSS_RC_OK;
}

void phy_init(void)
{
	int	i;
	u16	aux;
	u32	phy_id;

	/* Check if sysfs entries are available */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		phy_reg[i] = open_mdio_reg(i);
		phy_val[i] = open_mdio_val(i);

		/* Both sysfs files must be present;
		 * If not, consider the PHY invalid
		 */
		if (phy_reg[i] == -1 || phy_val[i] == -1) {
			phy_present[i] = FALSE;
			continue;
		}
		phy_present[i] = TRUE;

		/* Check if our PHY driver is compatible with the probed PHYs */
		if (l2sw_miim_read(NULL, i, PHY_ID_REG1, &aux) != VTSS_RC_OK) {
			phy_present[i] = FALSE;
			continue;
		}
		phy_id = (aux & 0xFFFF) << 16;
		if (l2sw_miim_read(NULL, i, PHY_ID_REG2, &aux) != VTSS_RC_OK) {
			phy_present[i] = FALSE;
			continue;
		}
		phy_id |= (aux & 0xFFFF);
		if (phy_id != PHY_ID_VSC8514 && phy_id != PHY_ID_VSC8234) {
			phy_present[i] = FALSE;
			printf("ERROR: Incompatible PHY found (PHY ID = 0x%08x) on port %d\n",
					phy_id, i);
			continue;
		}
		phy_present[i] = TRUE;
	}
}

void phy_cleanup(void)
{
	int	i;

	/* Close the opened file descriptors */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		if (phy_reg[i] >= 0) {
			close(phy_reg[i]);
			phy_reg[i] = -1;
		}
		if (phy_val[i] >= 0) {
			close(phy_val[i]);
			phy_val[i] = -1;
		}
		phy_present[i] = FALSE;
	}
}

vtss_rc l2sw_reg_read(const vtss_chip_no_t chip_no,
		const u32 addr,
		u32 *const value)
{

	/* Use the correct endianess */
	*value = __le32_to_cpu(base_mem[addr]);
	return VTSS_RC_OK;
}

vtss_rc l2sw_reg_write(const vtss_chip_no_t chip_no,
		const u32 addr,
		const u32 value)
{
	/* Use the correct endianess */
	base_mem[addr] = __cpu_to_le32(value);
	return VTSS_RC_OK;
}

/* Find the device associated with the Seville switch and save it in iodev */
bool uio_device_exists(char *path, size_t path_len)
{
	DIR		*dir;
	struct dirent	*dent;
	char		fn[PATH_MAX];
	char 		devname[64];
	FILE		*fp;
	bool		found = FALSE;

	if (!(dir = opendir(SYS_UIO_DIR)))
	{
		perror(SYS_UIO_DIR);
		exit(1);
	}

	while ((dent = readdir(dir)) != NULL)
	{
		if (strstr(dent->d_name, "uio") != NULL)
		{
			snprintf(fn, sizeof(fn), "%s/%s/name", SYS_UIO_DIR,
					dent->d_name);
			fp = fopen(fn, "r");
			if (fp)
			{
				fgets(devname, sizeof(devname), fp);
				fclose(fp);

				if (strstr(devname, SEVILLE_DEVNAME))
				{
					snprintf(path, path_len,
							"/dev/%s", dent->d_name);
					found = TRUE;
					break;
				}
			}
		}
	}
	closedir(dir);

	return found;
}

int get_next_value_base(const char **string, unsigned int *value, int base)
{
	char	*endptr;
	int	ret;

	errno = 0;
	ret = strtol(*string, &endptr, base);
	*value = (unsigned int)ret;

	/* Check if there were no digits at all */
	if (*string == endptr)
		return -1;
	*string = endptr;

	/* Check if an overflow or underflow occured;
	 * Check if a negative value was provided.
	 */
	return errno || ret < 0;
}

int get_next_value(const char **string, unsigned int *value)
{
	return get_next_value_base(string, value, 10);
}

/* get next IP address from a string */
int get_next_ip_address(const char **string, u32 *value)
{
	int i;
	unsigned int aux;
	const char *command;

	command  = *string;

	*value = 0;
	for (i = 0; i < 4; i++) {
		if (get_next_value(&command, &aux))
			return -1;

		*value += (u32)aux << ((3 - i) * 8);
		if (i < 3) {
			if (command[0] != '.')
				return -1;
			command++;
		}
	}

	*string = command;

	return 0;
}

/* Check if a port is also an internal port */
int is_internal_port(int port_no)
{
	if (port_no == 8 || port_no == 9)
		return TRUE;
	return FALSE;
}

/* check port number */
int is_valid_port(int port_no)
{
	if (port_no < VTSS_PORT_NO_START ||
			port_no >= VTSS_PORT_NO_END)
		return FALSE;
	return TRUE;
}

/* check VLAN value */
int is_valid_vid(int vid)
{
	if (vid < VTSS_VID_NULL || vid >= VTSS_VID_ALL) {
		printf("Please set a VID from [%d - %d]\n",
				VTSS_VID_NULL, VTSS_VID_ALL - 1);
		return FALSE;
	}
	return TRUE;
}

/* check Private VLAN value */
int is_valid_private_vid(const int pvlan)
{
	if (pvlan < VTSS_PVLAN_NO_START || pvlan >= VTSS_PVLAN_NO_END) {
		printf("Please set a Private VLAN from [%d - %d]\n",
				VTSS_PVLAN_NO_START, VTSS_PVLAN_NO_END - 1);
		return FALSE;
	}
	return TRUE;
}

/* Parse MAC address from a string, using
 * portable formatting macros
 */
int parse_mac(const char *mac_string, u8 addr[6])
{
	return sscanf(mac_string, MAC_ADDR_SCAN_FORMAT,
			MAC_ADDR_SCAN_ARGS(addr));
}

/* Parse OUI address from a string, using
 * portable formatting macros
 */
int parse_oui(const char *oui_string, u8 addr[OUI_ADDR_COUNT])
{
	return sscanf(oui_string, OUI_ADDR_SCAN_FORMAT,
			OUI_ADDR_SCAN_ARGS(addr));
}

/*
 * Stubs required by driver
 */
void vtss_callout_trace_printf(const vtss_trace_layer_t layer,
				const vtss_trace_group_t group,
				const vtss_trace_level_t level,
				const char *file,
				const int line,
				const char *function,
				const char *format,
				...)
{
	/* Implement for debug purposes*/
}

void vtss_callout_trace_hex_dump(const vtss_trace_layer_t layer,
				const vtss_trace_group_t group,
				const vtss_trace_level_t level,
				const char *file,
				const int line,
				const char *function,
				const unsigned char *byte_p,
				const int byte_cnt)
{
	/* Implement for debug purposes*/
}

void vtss_callout_lock(const vtss_api_lock_t *const lock)
{
	/* Implement for debug purposes*/
}

void vtss_callout_unlock(const vtss_api_lock_t *const lock)
{
	/* Implement for debug purposes*/
	/* Call after vtss_callout_lock */
}
