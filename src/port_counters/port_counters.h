/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _PORT_COUNTERS_H_
#define _PORT_COUNTERS_H_
/* port counters header */

#include <fsl_utils/fsl_utils.h>

int port_counters(int port_no, const char *command);

#endif /* _PORT_COUNTERS_H_ */

