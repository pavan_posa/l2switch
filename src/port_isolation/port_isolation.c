/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * port_isolation.c
 */

#include "port_isolation.h"

/* set isolation for a VLAN */
int vlan_isolation_set(int vid, BOOL isolate)
{
	if (!is_valid_vid(vid))
		return 1;

	if (vtss_isolated_vlan_set(NULL, vid, isolate) != VTSS_RC_OK) {
		printf("Can't set isolation for VLAN %d\n", vid);
		return 1;
	}
	return 0;
}

/* get isolation of a VLAN */
int vlan_isolation_get(int vid, BOOL *isolate)
{
	if (!is_valid_vid(vid))
		return 1;

	if (vtss_isolated_vlan_get(NULL, vid, isolate) != VTSS_RC_OK) {
		printf("Can't get isolation for VLAN %d\n", vid);
		return 1;
	}
	return 0;
}

/* show all isolated VLANs */
void vlan_isolation_show(void)
{
	int i;
	BOOL isolate;

	printf("Isolated VLANs: ");
	for (i = VTSS_VID_DEFAULT; i < VTSS_VIDS; i++)
		if (vlan_isolation_get(i, &isolate) == 0)
			if (isolate == TRUE)
				printf("%d, ", i);
	printf("\n");
}

/* add/remove a port from the isolated ports members */
int isolated_member_set(int port_no, BOOL enable)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_isolated_port_members_get(NULL, member) != VTSS_RC_OK) {
		printf("Couldn't get the isolated ports\n");
		return 1;
	}

	member[port_no] = enable;

	if (vtss_isolated_port_members_set(NULL, member) != VTSS_RC_OK) {
		printf("Couldn't set the isolated ports\n");
		return 1;
	}

	return 0;
}

/* get the isolated ports */
int isolated_members_get(BOOL member[VTSS_PORT_NO_END])
{
	int i;
	BOOL vtss_member[VTSS_PORT_ARRAY_SIZE];

	if (vtss_isolated_port_members_get(NULL, vtss_member) != VTSS_RC_OK) {
		printf("Couldn't get the isolated ports\n");
		return 1;
	}

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			member[i] = vtss_member[i];

	return 0;
}

/* clear all the isolated ports list */
int isolated_members_clear(void)
{
	int i;
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (vtss_isolated_port_members_get(NULL, member) != VTSS_RC_OK) {
		printf("Couldn't get the isolated ports\n");
		return 1;
	}

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			member[i] = FALSE;

	if (vtss_isolated_port_members_set(NULL, member) != VTSS_RC_OK) {
		printf("Couldn't set the isolated ports\n");
		return 1;
	}

	return 0;
}

/* show the list with the isolated ports */
void isolated_members_show(void)
{
	int i;
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (vtss_isolated_port_members_get(NULL, member) != VTSS_RC_OK) {
		printf("Couldn't get the isolated ports\n");
		return;
	}

	printf("Isolated ports: ");
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		if (is_valid_port(i) && (member[i] == TRUE))
			printf("%d, ", i);
	}
	printf("\n");
}
