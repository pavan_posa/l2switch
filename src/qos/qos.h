/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * qos.h
 */

#ifndef QOS_H_
#define QOS_H_

#include <fsl_utils/fsl_utils.h>

enum storm_control_type {
	STORM_CONTROL_UC,
	STORM_CONTROL_MC,
	STORM_CONTROL_BC
};

/* set/get storm control rate
 * use VTSS_PACKET_RATE_DISABLED to disable */
int storm_control_set(enum storm_control_type type, vtss_packet_rate_t rate);
int storm_control_get(enum storm_control_type type, vtss_packet_rate_t *rate);

/* show all storm control configuration */
void storm_control_show(void);

/* PCP/DEI rewriting */

/* get/set the PCP remark mode on the egress side
 * TODO: mode VTSS_TAG_REMARK_MODE_CLASSIFIED is not tested for now
 * we need to add IS1 entries to make this mode useful */
int port_pcp_remark_emode_set(int port_no, vtss_tag_remark_mode_t emode);
int port_pcp_remark_emode_get(int port_no, vtss_tag_remark_mode_t *emode);

/* get/set default egress PCP value
 * used only if port is in remark mode VTSS_TAG_REMARK_MODE_DEFAULT */
int port_pcp_egress_default_set(int port_no, vtss_tagprio_t pcp);
int port_pcp_egress_default_get(int port_no, vtss_tagprio_t *pcp);

/* get/set egress mapping form QoS and DP to a new PCP value
 * used only if port is in remark mode VTSS_TAG_REMARK_MODE_MAPPED */
int port_pcp_egress_map_set(int port_no, vtss_prio_t qos, vtss_dp_level_t dp,
		vtss_tagprio_t pcp);
int port_pcp_egress_map_get(int port_no, vtss_prio_t qos, vtss_dp_level_t dp,
		vtss_tagprio_t *pcp);

/* DSCP API */

/* run at init time */
void dscp_init(void);

/* ingress */
/* DSCP remark enable values
 * used only when mode is VTSS_DSCP_MODE_SEL */
int dscp_remark_ingress_val_set(vtss_dscp_t dscp, BOOL enable);
/* set DSCP ingress mode */
int port_dscp_remark_imode_set(int port_no, vtss_dscp_mode_t mode);
int port_dscp_remark_imode_get(int port_no, vtss_dscp_mode_t *mode); /* TODO */
/* map DSCP at ingress */
int port_dscp_translate_set(int port_no, BOOL enable);
int dscp_translate_map(vtss_dscp_t dscp_parsed, vtss_dscp_t dscp_classified);
/* set QoS based on DSCP (classified) value (per port) */
int port_qos_dscp_set(int port_no, BOOL enable);
/* set trusted DSCP values (per switch) */
int dscp_trust_set(vtss_dscp_t dscp_trust_val, BOOL enable);
/* set mapping from DSCP classified value to QoS class */
int qos_dscp_map(vtss_dscp_t dscp, vtss_prio_t qos);

/* egress DSCP */
/* remap DSCP at egress, depends on classified DP
 * TODO: these mapped values doesn't seem to be reset back to default values
 * after initialization, so initialization must be done at init */
int dscp_remap_set(vtss_dp_level_t dp, vtss_dscp_t dscp, vtss_dscp_t dscp_re);
int port_dscp_remark_emode_set(int port_no, vtss_dscp_emode_t mode);
int port_dscp_remark_emode_get(int port_no, vtss_dscp_emode_t *mode); /* TODO */
void qos_port_conf_show(void); /* TODO: add more details */

/* do configuration for L2_POLICER test */
int test_L2_POLICER_cfg(bool restore);

/* do configuration for L2_EGR_SHPR test */
int test_L2_EGR_SHPR_cfg(bool restore);

/* Configures per-port QoS classification based on PCP and DEI */
int qos_command_pcp(const char *command);

/* Configures per-port default QoS classification */
int qos_command_default(const char *command);

/* Configures a QCL to classify ICMP frames to specific QoS class.
 */
int test_L2_QCL_1(bool restore);

#endif /* QOS_H_ */
