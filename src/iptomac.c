#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* inttohex(char* , int );

int main()
{
   FILE *fp;
   fp = fopen("/home/tanmayi/Documents/DDP/git/src/ipaddress.txt", "r");
   int i;
   for(i=0;i<8192;i++)
   {
   	char *ip = malloc(20  * sizeof(char));
   	char line[100];
   	char *command = malloc(100  * sizeof(char));
   	strcat(command, "set multicast 01:00:5e");
   	fgets(line, sizeof(line), fp);
   	char *port = malloc(100  * sizeof(char));
   	strcpy(port, line);
   	strcat(ip, strtok(line, " "));
	while (*port != 0 && *(port++) != ' ') {}
   	char *token;
   	token = strtok(ip, ".");
   	int counter = 1;
   	while(token != NULL) 
   	{
   		if(counter != 1)
   		{
   			strcat(command, ":");
   			strcat(command, inttohex(token, counter));
   		}
      		token = strtok(NULL, ".");
      		counter++;
   	}
   	strcat(command, " ports ");
   	strcat(command, port);
   	printf("%s\n",command);
   	//multicast_command(command);
   	free(ip);
   	free(command);
   	free(token);
   }
   fclose(fp);
   return 0;
}

char* inttohex(char *token, int counter)
{
   int num;
   char *table[] = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f",
   		    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f",
   		    "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f",
   		    "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f",
		    "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f",
		    "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f",
		    "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f",
		    "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f",
		    "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f",
		    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f",
		    "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af",
		    "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf",
		    "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf",
		    "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df",
		    "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef",
		    "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff"};
   if(counter != 1)
   {
   	num = atoi(token);
   	if(counter == 2)
   	{
   		
   		if(num >= 128)
   		{
   			num -= 128;
   		}
   	}
   }
   return table[num];
}







/*Example All IPv4 multicast frames in VLAN 12 with MAC 01005E112233 are to be
forwarded to ports 3, 8, and 9. This is done by inserting the following entry in the MAC
table entry:
VALID = 1 VID = 12 MAC = 0x000308112233 ENTRY_TYPE = 2 DEST_IDX = 0







Set the MAC and VID in MACHDATA and MACLDATA
Set MACACCESS.ENTRY_TYPE = 1 because this is not an entry subject to aging.
Set MACACCESS.AGED_FLAG to 0.
Set MACACCESS.DEST_IDX to an unused value.
Set the destination mask in the destination mask table pointed to by DEST_IDX to
the desired ports.*/
